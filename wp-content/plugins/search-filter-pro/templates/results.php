<?php
/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 *
 */
?>
<script>
    jQuery(document).ready(function () {
        if (!jQuery('.sf-input-select').attr('data-combobox')) {
            jQuery(document).ajaxComplete(function () {
                jQuery('.sf-input-select').fancySelect();
            });
        }
        $( document ).ajaxStart(function() {
            if(jQuery('.filters-wrapper').hasClass('open'))
                jQuery('.filters-wrapper').children('form').hide();

            jQuery('.filters-wrapper').removeClass('open');
            $('.uil-rolling-css').remove();
            $( ".search-filter-results" ).append( '<svg class="uil-rolling-css" width="50px" height="50px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="42.5" stroke-dasharray="186.92476288859268 80.11061266653974" stroke="#e21f23" fill="none" stroke-width="15"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg>');
        });
    });

</script>
<?php
if ($query->have_posts()) : ?>

    <div class="results-wrapper">
        <h3>Znaleziono <?php echo $query->found_posts; ?> pasujących produktów<br/></h3>
        <?php while ($query->have_posts()) : $query->the_post(); ?>
            <?php
            if(get_field('produkt_archiwalny'))
                $labels = array('box', 'archived');
            else {
                $labels = get_field('etykiety');
                $labels = array('box', is_array($labels) ? $labels[0] : $labels);
            }
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class($labels); ?>>
                <a href="<?php the_permalink(); ?>">
                    <div class="image-wrapper">
                        <?php echo wp_get_attachment_image(get_field('zdjęcia')[0]['id'], 'product-list'); ?>
                    </div>
                    <h4><?php the_title(); ?></h4>
                </a>
                <button type="button" class="add_to_cart" onclick="addToCart(this)" data-id="<?php the_ID(); ?>">+ Zapytanie</button>
            </article>
        <?php endwhile; ?>
    </div>
    <?php
    $paged = $query->query['paged'];
    $paged == 1 && $is_first = true;
    $paged == $query->max_num_pages && $is_last = true;
    $base = $args['posts_per_page'];
    $p = min(max($paged - floor($base / 2), 1), $query->max_num_pages - $base + 1);

    $qs = '?';
    foreach ($_GET as $key => $get) {
        if ($key != 'sf_paged')
            $qs .= $key . '=' . $get . '&';
    }
    $qs = trim($qs, '&');
    ?>
    <?php if ($query->max_num_pages > 1) : ?>
        <div class="pagination">
            <!--            Strona --><?php //echo $query->query['paged']; ?><!-- z --><?php //echo $query->max_num_pages; ?><!--<br/>-->
            <div class="nav-first">
                <?php if (!$is_first) : ?>
                    <a href="<?php echo $qs; ?>"></a>
                <?php endif; ?>
            </div>
            <div class="nav-prev">
                <?php if ($paged > 1) : ?>
                    <a href="<?php echo $qs . '&sf_paged=' . ($paged - 1); ?>"></a>
                <?php endif; ?>
            </div>
            <?php for ($i = 0; $i < $base; $i++) : ?>
                <?php if ($p > 0) : ?>
                    <?php if ($p == $paged) : ?>
                        <span class="active"><?php echo $p; ?></span>
                    <?php else : ?>
                        <a href="<?php echo $qs . '&sf_paged=' . $p; ?>"><?php echo $p; ?></a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php $p++; ?>
            <?php endfor; ?>
            <div class="nav-next">
                <?php if ($paged < $query->max_num_pages) : ?>
                    <a href="<?php echo $qs . '&sf_paged=' . ($paged + 1); ?>"></a>
                <?php endif; ?>
            </div>
            <div class="nav-last">
                <?php if (!$is_last) : ?>
                    <a href="<?php echo $qs . '&sf_paged=' . $query->max_num_pages; ?>"></a>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

<?php else : ?>
    <div class="not-found">
        <div class="page-content">
            <img src="<?php echo get_template_directory_uri().'/images/not-found-article.png'?>"
            <h6>Nie znaleziono produktów o wybranych parametrach</h6>
        </div>
    </div>
<?php endif; ?>


