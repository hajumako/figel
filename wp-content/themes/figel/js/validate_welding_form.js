/**
 * Created by Konrado on 2016-09-08.
 */
jQuery(document).ready(function () {
    $ = jQuery;

    $next = $('.next-step');
    $nav = $('.nav-form ul li a');
    $stepWrappers = $('.step-wrapper');
    $globalErrors = $('.globalError');
    $userInputs = $('.userInput');
    $busy = $('#busy');
    $success = $('#success');
    $error = $('#error');
    sending = false;

    $errorStep2 = $('#errorStep2');
    $errorStep3 = $('#errorStep3');
    $method = $('input[name="method_type"]');
    $borders = $('.border');

    $userInputs.on('focus, click', function () {
        $(this).removeClass('error unfold');
        $('.error-msg', $(this).closest('.inputWrapper')).remove();
        $globalErrors.removeClass('show');
    });

    $nav.click(function (event) {
        var next = Number($(this).attr('data-step').replace('step', '')),
            curr = Number($('.step-wrapper.active').attr('id').replace('step', ''));
        if (stepIsValid() || next < curr) {
            $nav.removeClass('active');
            $(this).addClass('active');
            $stepWrappers.removeClass('active');
            $('#' + $(this).attr('data-step')).addClass('active');
        }
        event.preventDefault();
        return false;
    });

    $next.click(function () {
        if (stepIsValid()) {
            var step = $(this).attr('data-step');
            $nav.removeClass('active');
            $('.nav-form a[data-step="' + step + '"]').addClass('active');
            $stepWrappers.removeClass('active');
            $('#' + step).addClass('active');
        }
    });

    $method.change(function () {
        $('#con .conditional-show').removeClass('show-inline');
        $borders.removeClass('show');
        var val = $(this).val();
        if (val == 'welding') {
            $('#welding_methods').addClass('show-inline');
            $borders.addClass('show');
        } else if (val == 'soldering') {
            $('#soldering_methods').addClass('show-inline');
            $borders.addClass('show');
        }
    });

    $('input[name="welding_method"]').change(function () {
        if($('input[name="welding_method"]:checked').length > 2)
            this.checked = false;
    });

    function stepIsValid() {
        var step = $('.step-wrapper.active').attr('id'),
            val;

        $('.error-msg').remove();
        $globalErrors.removeClass('show');
        $userInputs.removeClass('error');

        if (step == 'step1') {
            val = $('input[name="process_type"]:checked').val();
            if (typeof val !== 'undefined') {
                $('#step2 .conditional-show').removeClass('show');
                $('#' + val).addClass('show');

                return true;
            }
            $('#' + step + ' div').append('<p class="error-msg">Wybierz proces</p>');

            return false;
        } else if (step == 'step2') {
            $errorStep2.removeClass('show');
            val = $('input[name="process_type"]:checked').val();
            var error = false,
                $thickness;

            if (val == 'con') {
                if (!$('input[name="con_need[]"]:checked').length) {
                    error = true;
                    $('input[name="con_need[]"]').closest('div').append('<p class="error-msg">Wybierz co najmniej jedną opcję</p>');
                }
                if (!$('input[name="con_material1_type[]"]:checked').length) {
                    error = true;
                    $('input[name="con_material1_type[]"]').closest('div').append('<p class="error-msg">Wybierz co najmniej jedną opcję</p>');
                }
                $thickness = $('input[name="con_material1_thickness"]');
                if ($thickness.val() <= 0 || $thickness.val() == '0' || $thickness.val() == '' || $thickness.val() == null) {
                    error = true;
                    $thickness.addClass('error');
                }
                if (typeof $('input[name="method_type"]:checked').val() === 'undefined') {
                    error = true;
                    $method.closest('div').append('<p class="error-msg">Wybierz co najmniej jedną opcję</p>');
                }
                
                if(error)
                    $errorStep2.addClass('show');

                return !error;
            } else if (val == 'cut') {
                if (!$('input[name="cut_need[]"]:checked').length) {
                    error = true;
                    $('input[name="cut_need[]"]').closest('div').append('<p class="error-msg">Wybierz co najmniej jedną opcję</p>');
                }
                if (!$('input[name="cut_material_type[]"]:checked').length) {
                    error = true;
                    $('input[name="cut_material_type[]"]').closest('div').append('<p class="error-msg">Wybierz co najmniej jedną opcję</p>');
                }
                $thickness = $('input[name="cut_material_thickness"]');
                if ($thickness.val() <= 0 || $thickness.val() == '0' || $thickness.val() == '' || $thickness.val() == null) {
                    error = true;
                    $thickness.addClass('error');
                }

                if(error)
                    $errorStep2.addClass('show');

                return !error;
            }

            return false;
        } else if (step == 'step3') {
            //submit call
            return true;
        }

        return false;
    }

    $('#form-figel').submit(function (event) {
        event.preventDefault();

        if (!sending) {
            $('.error-msg').remove();
            $userInputs.removeClass('error unfold');
            $globalErrors.removeClass('show');
            $success.removeClass('show');
            $error.removeClass('show');

            var error = false;

            $.each($('#step3 .userInput.required'), function (i, element) {
                if (element.value == 0 || element.value == '0' || element.value == '' || element.value == null) {
                    $(element).addClass('error');
                    error = true;
                } else if (element.id == 'phone') {
                    if (element.value.replace(/[^0-9]/g, '').length != 9) {
                        $(element).addClass('error unfold');
                        error = true;
                        $(element).after('<div class="error-msg">Nieprawidłowy format telefonu</div>');
                    }
                } else if (element.id == "email") {
                    if (/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(element.value) == false) {
                        $(element).addClass('error unfold');
                        error = true;
                        $(element).after('<div class="error-msg">Nieprawidłowy adres e-mail</div>');
                    }
                }
            });
            if ($('#g-recaptcha-response').val() == '') {
                error = true;
                $('.g-recaptcha').append('<div class="error-msg">Nieprawidłowa captcha</div>');
            }
            if (!error) {
                $busy.addClass('show');
                sending = true;
                $.post(
                        site_url + '/wp-admin/admin-ajax.php',
                    $(this).serialize(),
                    'json'
                    )
                    .done(function (data) {
                        try {
                            response = jQuery.parseJSON(data);
                            if (response.type == 'error') {
                                $.each(response.messages, function (i, msg) {
                                    $error.append(msg);
                                });
                                $error.addClass('show');
                            } else {
                                resetForm();
                                $success.show().delay(3000).fadeOut(300);
                            }
                        } catch (e) {
                            console.log(data);
                        }
                        $busy.removeClass('show');
                        sending = false;
                        grecaptcha.reset();
                    })
                    .error(function (data) {
                        $busy.removeClass('show');
                        $error.addClass('show').html('Błąd: ' + data.responseText);
                        sending = false;
                        grecaptcha.reset();
                    });
            } else {
                $errorStep3.addClass('show');
            }
        }
        return false;
    });

    function resetForm() {
        $('input[type="checkbox"]').prop('checked', false);
        $('input[type="radio"]').prop('checked', false);
        $('input[type="text"]').val('');
        $('input[type="number"]').val('');
        $('textarea').val('');
        $('.conditional-show').removeClass('show show-inline');
        $('span.holder').show();
        $nav.removeClass('active');
        $('.nav-form a[data-step="step1"]').addClass('active');
        $stepWrappers.removeClass('active');
        $('#step1').addClass('active');
    }

    $('span.holder + input, span.holder + textarea')
        .focusin(function () {
            $this = $(this);
            $this.prev('span.holder').hide();
            $this.removeClass('error unfold');
            $('.error-msg', $this.closest('.inputWrapper')).remove();
            $globalErrors.removeClass('show');
        })
        .focusout(function () {
            $this = $(this);
            if (!$this.val().length)
                $this.prev('span.holder').show();
        });

    $('span.holder').click(function () {
        $(this).next().focus();
    });
});

function recaptchaCallback() {
    $('.g-recaptcha .error-msg').remove();
}

function fillForm() {
    $('input[name="f_name"]').val('Test');
    $('input[name="f_company"]').val('Testowa');
    $('input[name="f_email"]').val('k.c.kucharski@gmail.com');
    $('input[name="f_phone"]').val('123456789');
}