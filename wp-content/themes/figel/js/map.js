

var map,
    markers = [],
    infowindow = null;
    templateUrl = object_name.templateUrl;
    image = templateUrl + '/images/PIN.png';
function initMap() {
    var mapOptions = {

        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative.province","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative.province","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#a0a0a0"},{"lightness":17},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#cecece"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#cecece"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]}]
    };


    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var bounds = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {
        addMarkerWithTimeout(i, i * 200);
        bounds.extend(new google.maps.LatLng(locations[i][0], locations[i][1]));
    }

    map.fitBounds(bounds);
}

function addMarkerWithTimeout(i, timeout) {


    window.setTimeout(function() {
        markers.push(new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][0], locations[i][1]),
            map: map,
            icon: image,
            animation: google.maps.Animation.DROP
        }));

        google.maps.event.addListener(markers[i], 'click', (function (marker, i) {
            return function () {
                if(infowindow)
                    infowindow.close();
                infowindow = new google.maps.InfoWindow();
                infowindow.setContent(locations[i][2]);
                infowindow.open(map, marker);
            }
        })(markers[i], i));
    }, timeout);
}

jQuery(document).ready(function () {
    $ = jQuery;
    $('#offices li').hover(function () {
        var t = $(this).attr('data-target');
        if (markers[t].getAnimation() !== null) {
            markers[t].setAnimation(null);
        } else {
            markers[t].setAnimation(google.maps.Animation.BOUNCE);
        }
    });
});