// ilosc pprzesuniec w prawo
var $currentMenuPosition = 0;
//var $currentMenuId = 0;
var $lastMenuPosition = 0;
var $menuStepSize = 0;
var previous_width = 0;
var previous_num_boxes = 0;

jQuery(document).ready(function ($) {
    previous_width = $(window).width();

    //slides
    if(previous_width >= 1250)
    {
       var maxSlides = 5;
       var slideWidth = 210;
    }
    else if(previous_width < 1250 && previous_width >= 850){
        var maxSlides = 4;
        var slideWidth = 180;
    }
    else if(previous_width < 850 && previous_width >= 500)
    {
        var maxSlides = 2;
        var slideWidth = 150;
    }
    else if(previous_width < 500){
        var maxSlides = 1;
        var slideWidth = 200;
    }

    previous_width = window.innerWidth;

    //logotypes in footer
    var logotypesBxslider = $('.bxslider');
    logotypesBxslider.css('display', 'block');
    logotypesBxslider.bxSlider({
        useCSS: false,
        easing: 'easeOutQuint',
        slideWidth: slideWidth,
        minSlides: 2,
        maxSlides: maxSlides,
        moveSlides: 1,
        slideMargin: 20,
        auto: true,
        pause: 5000,
        autoHover: true,
        speed: 0,
        pager: false,
        onSlideBefore: function () {
            logotypesBxslider.fadeOut();
        },
        onSlideAfter: function () {
            logotypesBxslider.fadeIn();
        }
    });

    // recent news in main page
    var recentNewsBxslider = $('.bxslider-recent-news');
    recentNewsBxslider.css('display', 'block');
    recentNewsBxslider.bxSlider({
        controls: false,
        auto: true,
        speed: 1000,
    });

    //slider in content flex
    var newsBxslider = $('.bxslider-news');
    newsBxslider.css('display', 'block');
    $('.bx-pager-news').css('display', 'block');
    var slider = newsBxslider.bxSlider({
        auto: true,
        controls: true,
        pager: false,
    });

    var sliderPager = $(".bx-pager-news").bxSlider({
        minSlides: 1,
        maxSlides: 4,
        slideWidth: 140,
        slideMargin: 5,
        moveSlides: 1,
        auto: false,
        pager: false,
        controls: true,
        nextText: 'Next',
        prevText: 'Prev',
        infiniteLoop: false,

    });
    $('.bx-pager-news li').on('click', 'a', function(e){
        e.preventDefault();
        slider.stopAuto();
        slider.goToSlide($(this).attr('data-slideIndex'));
    });

    // SEARCH
    $('.search-btn-cover').on('click', function () {

        $('.search-form').addClass('open-search');
        $('.body-bg').addClass('active-btn-bg');
        $('body').addClass('no-scroll');
        //jQuery('.search-container').addClass('active-search-container');
        $('.search-btn-cover').addClass('inactive-btn-cover');

        setTimeout(function () {
            $('.search-field').trigger('focus');

        }, 600);
    });

    $('.clear').on('click', function () {
        $('.search-field').val('');

    });

    jQuery(document).on('click', function(e){
        var searchContainer = $('.search-field');
       // var searchBtnBg =  jQuery('.pla_search');
        var searchSubmitBtn = $('input[type="submit"]');
        var searchBtnCover =  $('.search-btn-cover');
        var search = $('#search-navigation');
        var $clear =  $('.clear');

        if($('.open-search').length != 0) {

            if (!searchContainer.is(e.target)  && !searchBtnCover.is(e.target) && !$clear.is(e.target) ) {

                $('.search-form').removeClass('open-search');
                $('.body-bg').removeClass('active-btn-bg');
                $('body').removeClass('no-scroll');
                $('.search-btn-cover').removeClass('inactive-btn-cover');
            }
        }
    });

    $(document).on('keyup', function (e) {

        if (($('#search-navigation').hasClass('open-search')) && (e.keyCode == 27)) {
            $('#search-navigation').removeClass('open-search');
            $('.body-bg').removeClass('active-btn-bg');
            $('body').removeClass('no-scroll');
            $('.search-btn-cover').removeClass('inactive-btn-cover');
        }
    });

    //MENU
    var timeoutID;
    $('.sub-menu').css('display', 'block');
    var $submenuWrapper = $('#main-navigation .top-menu > li > .sub-menu');

    //var $submenuWrapper = $($submenuWrappers[wrapId]);

    $submenuWrapper.parent().mouseenter(function () {
        clearTimeout(timeoutID);
        $('.body-bg').stop();
        $('.body-bg').addClass('active-btn-bg-menu');
    });
    $submenuWrapper.parent().mouseleave(function () {
        timeoutID = setTimeout(function () {
            $('.body-bg').removeClass('active-btn-bg-menu')
        }, 400);
    });

    var $menuCells = $submenuWrapper.find("> li");
    var $numCells = $menuCells.length;

    $menuCells.wrapAll($('<div class="cells-wrapper"></div>'));
    $submenuWrapper.wrapAll($('<div class="submenu-wrapper"></div>'));

    var $upperColumnCount = 0;
    var $elementsOnDesktop = 0;
    var $boxWidth = 0;
    console.log(previous_width);
    if ((previous_width <= 1366 ) ) {
        $upperColumnCount = 3;
        $elementsOnDesktop = 3;
        $menuStepSize = 3;
        $lastMenuPosition = Math.ceil($numCells / $menuStepSize) - 1;
        $boxWidth = 350;
        setElementsOnOfferMenu();
        console.log('test1');
    }
    else if ((previous_width > 1366 ) ) {
        $elementsOnDesktop = 8;
        $menuStepSize = 4;
        $lastMenuPosition = Math.ceil($numCells / 2 / $menuStepSize) - 1;
        $boxWidth = 305;
        if ($numCells > 8) {
            $upperColumnCount = Math.ceil($numCells / 2);
            console.log('test2');
        }
        else if ($numCells > 4) {
            $upperColumnCount = 4;
            console.log('test3');
        }
        setElementsOnOfferMenu();
    }

    function setElementsOnOfferMenu() {
        $('.prev').remove();
        $('.next').remove();

        var $elements = [];


        for (var element = 0; element < $upperColumnCount; element++) {
            $elements[element] = $menuCells[element];
        }
        if($('.cells-row-wrapper').length != 0){
            $($elements).unwrap();
        }


        $($elements).wrapAll($('<div class="cells-row-wrapper"></div>'));

        $elements = [];

        for (var element = $upperColumnCount; element < $numCells; element++) {
            $elements[element - $upperColumnCount] = $menuCells[element];
        }

        $($elements).wrapAll($('<div class="cells-row-wrapper"></div>'));


        if ($numCells > $elementsOnDesktop) {

            function renderItems(instantAnimation) {
                $('.cells-wrapper').stop();

                if (instantAnimation) {
                    $('.cells-wrapper').css("left", -$boxWidth * $currentMenuPosition * $menuStepSize + "px");
                }
                else {
                    $('.cells-wrapper').animate({
                        left: -$boxWidth * $currentMenuPosition * $menuStepSize + "px",
                    }, 1000, function () {
                        // Animation complete.
                    });
                }

                if ($currentMenuPosition == 0) {
                    $('.prev').css("display", "none");
                }
                else {
                    $('.prev').css("display", "block");
                }

                if ($currentMenuPosition == $lastMenuPosition) {
                    $('.next').css("display", "none");
                }
                else {
                    $('.next').css("display", "block");
                }
            }

            function previousItems() {
                if ($currentMenuPosition > 0) {
                    $currentMenuPosition--;

                    renderItems(false);
                }
            }

            function nextItems() {
                if ($currentMenuPosition < $lastMenuPosition) {
                    $currentMenuPosition++;

                    renderItems(false);
                }
            }

            //$lastMenuPosition[wrapId] = Math.ceil($numCells / 2) - 4;
            $('#main-navigation .top-menu > li > .submenu-wrapper').append($('<div class="prev"></div>'));
            $('#main-navigation .top-menu > li > .submenu-wrapper').append($('<div class="next"></div>'));

            $('.prev').on("click", previousItems);
            $('.next').on("click", nextItems);

            renderItems(true);

            var $menuItem = $submenuWrapper.parent().parent().parent();
            $menuItem.on('mouseenter', function () {
                //$currentMenuId = $submenuWrappers.index($(this).find("> .submenu-wrapper > .sub-menu")[0]);
                $currentMenuPosition = 0;
                renderItems(true);

            });
        }
    }

    //mmenu
    $("#menu-mobile").mmenu({
        "header": true,
        "searchfield": false,
        "offCanvas": {
            "position": "right",
            "zposition": "front"
        }
    });

    var API = $("#menu-mobile").data( "mmenu" );
    $(".mm-close-menu").click(function() {
        API.close();
    });


    var waypoint = new Waypoint({
        element: document.getElementById('banner'),
        handler: function (direction) {
            //console.log(direction);
            if (direction === "down") {   // Przyklejanie headera

                jQuery('.form-laczenia-ciecia').addClass('form-hide');

            }
            else if (direction === "up") {   // Odklejanie headera

                jQuery('.form-laczenia-ciecia').removeClass('form-hide');
            }
        },
        offset: 50
    });

    //END MENU

    function countOfferBoxes(){
        var boxes = $('.offer-list .box');
        var boxWidth = 270;

        var offerWrapperWidth = $('.content-wrapper:not(.products)').width();
        var numBoxesInRow = Math.floor((offerWrapperWidth - 66) / boxWidth);
        if(numBoxesInRow == 0)
            numBoxesInRow = 1;

        if(numBoxesInRow != previous_num_boxes) {
            if ($('.offer:not(.products) .offer-list .offer-border').length != 0)
                $('.offer:not(.products) .offer-list .offer-border').remove();

            for (var boxId = 1; boxId < boxes.length; boxId++)
                if (boxId % numBoxesInRow == 0)
                    $(boxes[boxId]).before('<div class="offer-border"></div>');

            previous_num_boxes = numBoxesInRow;
        }
    }

    function countProductsBoxes(){
        var boxes = $('.offer-list .box');
        var boxWidth = 270;


        var offerWrapperWidth = $('div.products .offer-list').width();
        var numBoxesInRow = Math.floor((offerWrapperWidth) / boxWidth);

        boxes.css({'display':'table-cell'});
        if(numBoxesInRow == 0)
            numBoxesInRow = 1;

        if(numBoxesInRow != previous_num_boxes) {
            if ($('.products .offer-list .offer-border').length != 0)
                $('.products .offer-list .offer-border').remove();
            if ($('.products .offer-list .offer-row').length != 0)
                boxes.unwrap();

            var logotypes_row = [boxes[0]];
            for (var boxId = 1; boxId < boxes.length; boxId++) {
                if (boxId % numBoxesInRow == 0) {
                    $(logotypes_row).wrapAll('<div class="offer-row"></div>');
                    logotypes_row = [];
                }
                logotypes_row[boxId % numBoxesInRow] = boxes[boxId];
            }

            if (logotypes_row.length) {
                $(logotypes_row).wrapAll('<div class="offer-row"></div>');
                logotypes_row = [];
            }

            previous_num_boxes = numBoxesInRow;
        }
    }

    function countCerfificatesBoxes (){
        var boxWidth = 270;

        var logotypesWrapperWidth = $('.certificates').parent().width();
        var numBoxesInRow = Math.floor((logotypesWrapperWidth) / boxWidth);
        if(numBoxesInRow == 0)
            numBoxesInRow = 1;


        if(numBoxesInRow != previous_num_boxes) {
            if ($('.certificates .offer-border').length != 0)
                $('.certificates .offer-border').remove();

            var logotypes = $('.certificates .box');
            for (var logotypeId = 1; logotypeId < logotypes.length; logotypeId++)
                if (logotypeId % numBoxesInRow == 0)
                    $(logotypes[logotypeId]).before('<div class="offer-border"></div>');

            previous_num_boxes = numBoxesInRow;
        }
    }

    //bordery w ofercie
    if($('.offer:not(.products)').length !=0)
        countOfferBoxes();

    //bordery w produktach
    if($('div.products').length != 0)
        countProductsBoxes();

    //bordery w certyfikatach
    if ($('.certificates').length != 0)
        countCerfificatesBoxes();

    //history
    $('.history .date').last().addClass('last-date');

    $(window).resize(function () {
        var current_width = window.innerWidth;

        if ((previous_width > 1366 && current_width > 1200 && current_width <= 1366) ) {
            $upperColumnCount = 3;
            $elementsOnDesktop = 3;
            $menuStepSize = 3;
            $lastMenuPosition = Math.ceil($numCells / $menuStepSize) - 1;
            $boxWidth = 350;
            setElementsOnOfferMenu();

        }
        else if ((previous_width <= 1366 && current_width > 1366) ) {
            $elementsOnDesktop = 8;
            $boxWidth = 305;
            $menuStepSize = 4;
            $lastMenuPosition = Math.ceil($numCells / 2 / $menuStepSize) - 1;
            if ($numCells > 8) {
                $upperColumnCount = Math.ceil($numCells / 2);
            }
            else if ($numCells > 4) {
                $upperColumnCount = 4;
            }
            setElementsOnOfferMenu();
        }

         if($('div.products').length != 0)
            countProductsBoxes();

        if($('.offer:not(.products) .offer-list').length != 0)
            countOfferBoxes();

        if($('.certificates').length != 0)
            countCerfificatesBoxes();

        previous_width = current_width;
    });
    jQuery(document).ajaxComplete(function () {
        if($('div.products').length != 0) {
            var boxes = $('.offer-list .box');
            var boxWidth = 270;


            var offerWrapperWidth = $('div.products .offer-list').width();
            var numBoxesInRow = Math.floor((offerWrapperWidth) / boxWidth);

            boxes.css({'display': 'table-cell'});
            if (numBoxesInRow == 0)
                numBoxesInRow = 1;


            if ($('.products .offer-list .offer-border').length != 0)
                $('.products .offer-list .offer-border').remove();
            if ($('.products .offer-list .offer-row').length != 0)
                boxes.unwrap();

            var logotypes_row = [boxes[0]];
            for (var boxId = 1; boxId < boxes.length; boxId++) {
                if (boxId % numBoxesInRow == 0) {
                    $(logotypes_row).wrapAll('<div class="offer-row"></div>');
                    logotypes_row = [];
                }
                logotypes_row[boxId % numBoxesInRow] = boxes[boxId];
            }

            if (logotypes_row.length) {
                $(logotypes_row).wrapAll('<div class="offer-row"></div>');
                logotypes_row = [];
            }


        }

    });

    $('.filters-wrapper h4').click(function(){
        $(this).parent().children('form').slideToggle();
        $(this).parent().toggleClass('open');
    });
});








