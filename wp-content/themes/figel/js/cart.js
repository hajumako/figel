/**
 * Created by Konrado on 2016-08-24.
 */

jQuery(document).ready(function () {
    jQuery('.delete_from_cart').click(deleteFromCart);
});

function addToCart(el) {
    var id = jQuery(el).attr('data-id'),
        ids = getCookie('figel_cart'),
        ids_a = ids.split(',');
    if(ids_a.indexOf(id) > -1)
        setCookie('figel_cart', ids, 2);
    else
        setCookie('figel_cart', ids.length ? ids + ',' + id : id, 2);
    updateCart(getCookie('figel_cart').split(',').length);
}

function deleteFromCart() {
    var ids = getCookie('figel_cart').replace(jQuery(this).attr('data-id'), '').replace(',,', ',').replace(/^,|,$/g, '');
    setCookie('figel_cart', ids, ids == '' ? -2 : 2);
    updateCart(ids.length ? ids.split(',').length : 0);
    jQuery(this).parent().remove();
}

function updateCart(cnt) {
    jQuery('.cart p').text(cnt + ' ' + ' produkt' + getSuffix(cnt));
    if(cnt > 0)
        jQuery('.cart').show();
    else
        jQuery('.cart').hide();
}

function clearCart() {
    setCookie('figel_cart', '', -2);
    updateCart(0);
    jQuery('.product-list ul li').remove();
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getSuffix(c) {
    if (c > 1) {
        c %= 100;
        if (c == 1) {
            return 'ów';
        } else if (c >= 2 && c <= 4) {
            return 'y';
        } else if (c >= 5 && c <= 21) {
            return 'ów';
        } else {
            c %= 10;
            if (c >= 2 && c <= 4) {
                return 'y';
            } else {
                return 'ów';
            }
        }
    } else {
        return '';
    }
}