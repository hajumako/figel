jQuery(document).ready(function($){
    //slider in product details
    var productBxslider = $('.bxslider-product');

    productBxslider.css('display', 'block');
    $(".bx-pager-product").css('display', 'block');

    var sliderProduct = productBxslider.bxSlider({
        auto: true,
        controls: true,
        pager: false,
        caption: true,
    });

    var sliderPagerProduct = $(".bx-pager-product").bxSlider({
        mode: 'vertical',
        minSlides:4,
        // maxSlides: 4,
        slideWidth: 100,
        slideMargin: 10,
        moveSlides: 1,
        auto: false,
        pager: false,
        controls: false,
        nextText: 'Next',
        prevText: 'Prev',
        infiniteLoop: false,

    });

    $('.bx-pager-product li').on('click', 'a', function(e){
        e.preventDefault();
        sliderProduct.stopAuto();
        sliderProduct.goToSlide($(this).attr('data-slideIndex'));
    });

    //Tabs
    var $productTab = $('.tabbed-content li.tab');
    var $tabContent = $('.tabbed-content > div');

    $productTab.first().addClass('active');
    $tabContent.first().addClass('active');

    $productTab.click(function () {
        var tab_id = $(this).attr('data-tab');

        $productTab.removeClass('active');
        $tabContent.removeClass('active');

        $(this).addClass('active');
        $("#" + tab_id).addClass('active');
    })


});