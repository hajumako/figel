<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="search-btn-cover"></div>
	<label>
<!--		<span class="screen-reader-text">--><?php //echo _x( 'Search for:', 'label', 'twentysixteen' ); ?><!--</span>-->

		<input type="search" class="search-field" placeholder="<?php pll_e('Wpisz szukane wyrażenie'); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		<div class="clear"><?php pll_e('wyszczyść');?></div>
	</label>
	<input type="submit" value="" />
</form>
