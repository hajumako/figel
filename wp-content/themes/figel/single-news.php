<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

$redirect = get_field('redirect', $post->ID);
if($redirect)
    header('Location: ' . $redirect);

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php get_template_part('template-parts/side', 'recent'); ?>

        <div class="content-wrapper news-details">

            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <p class="date"><?php the_date(); ?></p>

                            <?php foreach (wp_get_post_terms(get_the_ID(), 'news_type') as $term) : ?>
                                <h5 class="etykieta <?php echo $term->slug; ?>"><?php echo $term->name; ?></h5>
                            <?php endforeach; ?>
                            <div class="share-button-wrapper">
                                <p>Udostępnij</p>
                                <div class="buttons">
                                    <div class="facebook" data-href="http://figel.quellio.com"></div>
                                    <div class="google"><a href="https://plus.google.com/share?url=<?php print(urlencode(get_permalink())); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a></div>
                                    <div class="mail"><a href="mailto:?subject=<?php the_title() ;?>&amp;body=<?php echo get_the_permalink();?>"></a></div>
                                </div>
                            </div>
                        </header>
                          <div class="news-content">

                            <?php get_template_part('template-parts/content', 'flex'); ?>
                        </div>
                    </article>
                <?php endwhile; ?>
            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
