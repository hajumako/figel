<?php

add_action('init', 'init_cpt');
add_filter('manage_posts_columns', 'figel_columns_head');
add_action('manage_posts_custom_column', 'figel_columns_content', 10, 2);

add_filter('manage_edit-welding_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-welding_material_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_material_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-welding_handle_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_handle_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-plasma_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_plasma_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-gas_welding_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_gas_welding_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-helping_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_helping_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-welding_chemical_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_chemical_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-abrasive_material_type_columns', 'figel_taxonomy_columns');
add_filter('manage_abrasive_material_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-soldering_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_soldering_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-personal_protection_type_columns', 'figel_taxonomy_columns');
add_filter('manage_personal_protection_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-dust_exhaust_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_dust_exhaust_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-welding_protection_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_protection_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-welding_automation_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_automation_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-welding_robot_type_columns', 'figel_taxonomy_columns');
add_filter('manage_welding_robot_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-control_device_type_columns', 'figel_taxonomy_columns');
add_filter('manage_control_device_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

add_filter('manage_edit-auto_warehouse_type_columns', 'figel_taxonomy_columns');
add_filter('manage_auto_warehouse_type_custom_column', 'figel_taxonomy_columns_content', 10, 3);

function init_cpt()
{
//Aktualności

    $labels = array(
        'name' => 'Aktualności',
        'singular_name' => 'Aktualność',
        'menu_name' => 'Aktualności',
        'add_new' => 'Dodaj nową',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'aktualnosc',
            'with_front' => false
        ),
        'taxonomy' => 'news_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-megaphone',
        'menu_position' => 4,
        'supports' => array(
            'title',
            'author',
            'thumbnail',
            'excerpt'
        ),
        'capability_type' => array('news', 'newses'),
        'map_meta_cap' => true
    );
    register_post_type('news', $args);

    $args = array(
        'labels' => array(
            'name' => 'Kategorie aktualności',
            'menu_name' => 'Kategorie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'aktualnosci',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_news_type'
        )
    );
    register_taxonomy('news_type', 'news', $args);

//Artykuły

    $labels = array(
        'name' => 'Artykuły',
        'singular_name' => 'Artykuł',
        'menu_name' => 'Wiedza - artykuły',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'wiedza/artykul',
            'with_front' => false
        ),
        'has_archive' => true,
        'menu_icon' => 'dashicons-megaphone',
        'menu_position' => 4,
        'supports' => array(
            'title',
            'author',
            'thumbnail',
            'excerpt'
        ),
        'capability_type' => array('article', 'articles'),
        'map_meta_cap' => true
    );
    register_post_type('article', $args);


//Technologie

    $labels = array(
        'name' => 'Technologie',
        'singular_name' => 'Technologie',
        'menu_name' => 'Wiedza - technologie',
        'add_new' => 'Dodaj nową',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'wiedza/technologia-spawania',
            'with_front' => false
        ),
        'taxonomy' => 'technology_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-megaphone',
        'menu_position' => 4,
        'supports' => array(
            'title',
            'author',
            'thumbnail',
            'excerpt'
        ),
        'capability_type' => array('technology', 'technologies'),
        'map_meta_cap' => true
    );
    register_post_type('technology', $args);

    $args = array(
        'labels' => array(
            'name' => 'Kategorie technologii',
            'menu_name' => 'Kategorie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'wiedza/przeglad-technologii-spawania/',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_technology_type'
        )
    );
    register_taxonomy('technology_type', 'technology', $args);

//Oferty pracy
    $labels = array(
        'name' => 'Oferty pracy',
        'singular_name' => 'Oferta pracy',
        'menu_name' => 'Oferty pracy',
        'add_new' => 'Dodaj nową',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'praca/oferta-pracy',
            'with_front' => false
        ),
        'has_archive' => true,
        'menu_icon' => 'dashicons-hammer',
        'menu_position' => 4,
    );
    register_post_type('job_offer', $args);

//Menadżerowie produktów

    $labels = array(
        'name' => 'Menadżerowie produktów',
        'singular_name' => 'Menadżer produktu',
        'menu_name' => 'Menadżerowie produktów',
        'add_new' => 'Dodaj nowego',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'has_archive' => false,
        'menu_icon' => 'dashicons-businessman',
        'menu_position' => 20
    );
    register_post_type('product_manager', $args);
    
//Marki

    $labels = array(
        'name' => 'Marki',
        'singular_name' => 'Marka',
        'menu_name' => 'Marki',
        'add_new' => 'Dodaj nową'
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-admin-site',
        'menu_position' => 20
    );
    register_post_type('logotypes', $args);


//Oddziały

    $labels = array(
        'name' => 'Oddziały',
        'singular_name' => 'Oddział',
        'menu_name' => 'Oddziały',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'has_archive' => false,
        'menu_icon' => 'dashicons-location',
        'menu_position' => 21
    );
    register_post_type('office', $args);


//Pracownicy

    $labels = array(
        'name' => 'Pracownicy',
        'singular_name' => 'Pracownik',
        'menu_name' => 'Pracownicy',
        'add_new' => 'Dodaj nowego',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'has_archive' => false,
        'menu_icon' => 'dashicons-groups',
        'menu_position' => 21
    );
    register_post_type('employee', $args);


//Urządzenia do spawania

    $labels = array(
        'name' => 'Urządzenia do spawania',
        'singular_name' => 'Urządzenie do spawania',
        'menu_name' => 'Urządzenia do spawania',
        'add_new' => 'Dodaj nowe',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-do-spawania/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_device', 'welding_devices'),
        'map_meta_cap' => true
    );
    register_post_type('welding_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Urządzenia do spawania',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-do-spawania',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_device_type', 'welding_device', $args);


// Materiały spawalnicze

    $labels = array(
        'name' => 'Materiały spawalnicze',
        'singular_name' => 'Materiał spawalniczy',
        'menu_name' => 'Materiały spawalnicze',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/materialy-spawalnicze/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_material_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_material', 'welding_materials'),
        'map_meta_cap' => true
    );
    register_post_type('welding_material', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Materiały spawalnicze',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/materialy-spawalnicze',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_material_type', 'welding_material', $args);


//Uchwyty spawalnicze

    $labels = array(
        'name' => 'Uchwyty spawalnicze',
        'singular_name' => 'Uchwyt spawalniczy',
        'menu_name' => 'Uchwyty spawalnicze',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/uchwyty-spawalnicze/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_handle_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_handle', 'welding_handles'),
        'map_meta_cap' => true
    );
    register_post_type('welding_handle', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Uchwyty spawalnicze',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/uchwyty-spawalnicze',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_handle_type', 'welding_handle', $args);


//Urządzenia i osprzęt do cięcia plazmowego

    $labels = array(
        'name' => 'Urządzenia i osprzęt do cięcia plazmowego',
        'singular_name' => 'Urządzenie i osprzęt do cięcia plazmowego',
        'menu_name' => 'Urządzenia i osprzęt do cięcia plazmowego',
        'add_new' => 'Dodaj nowe',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array('slug' => 'oferta/urzadzenia-i-osprzet-do-ciecia-plazmowego/produkt', 'with_front' => false),
        'taxonomy' => 'plasma_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('plasma_device', 'plasma_devices'),
        'map_meta_cap' => true
    );
    register_post_type('plasma_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Urządzenia i osprzęt do cięcia plazmowego',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-i-osprzet-do-ciecia-plazmowego',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('plasma_device_type', 'plasma_device', $args);


//Osprzęt do spawania i cięcia gazowego

    $labels = array(
        'name' => 'Osprzęt do spawania i cięcia gazowego',
        'singular_name' => 'Osprzęt do spawania i cięcia gazowego',
        'menu_name' => 'Osprzęt do spawania i cięcia gazowego',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/osprzet-do-spawania-i-ciecia-gazowego/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'gas_welding_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('gas_welding_device', 'gas_welding_devices'),
        'map_meta_cap' => true
    );
    register_post_type('gas_welding_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Osprzęt do spawania i cięcia gazowego',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/osprzet-do-spawania-i-ciecia-gazowego',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('gas_welding_device_type', 'gas_welding_device', $args);


//Osprzęt pomocniczy

    $labels = array(
        'name' => 'Osprzęt pomocniczy',
        'singular_name' => 'Osprzęt pomocniczy',
        'menu_name' => 'Osprzęt pomocniczy',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/osprzet-pomocniczy/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'helping_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('helping_device', 'helping_devices'),
        'map_meta_cap' => true
    );
    register_post_type('helping_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Osprzęt pomocniczy',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/osprzet-pomocniczy',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('helping_device_type', 'helping_device', $args);


//Chemia spawalnicza

    $labels = array(
        'name' => 'Chemia spawalnicza',
        'singular_name' => 'Chemia spawalnicza',
        'menu_name' => 'Chemia spawalnicza',
        'add_new' => 'Dodaj nową',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/chemia-spawalnicza/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_chemical_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_chemical', 'welding_chemicals'),
        'map_meta_cap' => true
    );
    register_post_type('welding_chemical', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Chemia spawalnicza',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/chemia-spawalnicza',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_chemical_type', 'welding_chemical', $args);


//Materiały ścierne

    $labels = array(
        'name' => 'Materiały ścierne',
        'singular_name' => 'Materiał ścierny',
        'menu_name' => 'Materiały ścierne',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/materialy-scierne/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'abrasive_material_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('abrasive_material', 'abrasive_materials'),
        'map_meta_cap' => true
    );
    register_post_type('abrasive_material', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Materiały ścierne',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/materialy-scierne',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('abrasive_material_type', 'abrasive_material', $args);


//Urządzenia i osprzęt do zgrzewania

    $labels = array(
        'name' => 'Urządzenia i osprzęt do zgrzewania',
        'singular_name' => 'Urządzenie i osprzęt do zgrzewania',
        'menu_name' => 'Urządzenia i osprzęt do zgrzewania',
        'add_new' => 'Dodaj nowe',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-i-osprzet-do-zgrzewania/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'soldering_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('soldering_device', 'soldering_devices'),
        'map_meta_cap' => true
    );
    register_post_type('soldering_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Urządzenia i osprzęt do zgrzewania',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-i-osprzet-do-zgrzewania',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('soldering_device_type', 'soldering_device', $args);


//Środki ochrony indywidualnej

    $labels = array(
        'name' => 'Środki ochrony indywidualnej',
        'singular_name' => 'Środek ochrony indywidualnej',
        'menu_name' => 'Środki ochrony indywidualnej',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/srodki-ochrony-indywidualnej/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('personal_protection', 'personal_protections'),
        'map_meta_cap' => true
    );
    register_post_type('personal_protection', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Środki ochrony indywidualnej',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/srodki-ochrony-indywidualnej',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('personal_protection_type', 'personal_protection', $args);


//Odciągi pyłów i dymów

    $labels = array(
        'name' => 'Odciągi pyłów i dymów',
        'singular_name' => 'Odciąg pyłów i dymów',
        'menu_name' => 'Odciągi pyłów i dymów',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/odciagi-pylow-i-dymow/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'dust_exhaust_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('dust_exhaust_device', 'dust_exhaust_devices'),
        'map_meta_cap' => true
    );
    register_post_type('dust_exhaust_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Odciągi pyłów i dymów',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/odciagi-pylow-i-dymow',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('dust_exhaust_device_type', 'dust_exhaust_device', $args);


//Ochrona stanowiska spawalniczego

    $labels = array(
        'name' => 'Ochrona stanowiska spawalniczego',
        'singular_name' => 'Ochrona stanowiska spawalniczego',
        'menu_name' => 'Ochrona stanowiska spawalniczego',
        'add_new' => 'Dodaj nową',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/ochrona-stanowiska-spawalniczego/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_protection_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_protection', 'welding_protections'),
        'map_meta_cap' => true
    );
    register_post_type('welding_protection', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Ochrona stanowiska spawalniczego',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/ochrona-stanowiska-spawalniczego',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_protection_type', 'welding_protection', $args);


//Urządzenia do mechanizacji i automatyzacji spawania i cięcia

    $labels = array(
        'name' => 'Urządzenia do mechanizacji i automatyzacji spawania i cięcia',
        'singular_name' => 'Urządzenie do mechanizacji i automatyzacji spawania i cięcia',
        'menu_name' => 'Urządzenia do mechanizacji spawania',
        'add_new' => 'Dodaj nowe',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-do-mechanizacji-i-automatyzacji-spawania-i-ciecia/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_automation_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_automation', 'welding_automations'),
        'map_meta_cap' => true
    );
    register_post_type('welding_automation', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Urządzenia do mechanizacji i automatyzacji spawania i cięcia',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-do-mechanizacji-i-automatyzacji-spawania-i-ciecia',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_automation_type', 'welding_automation', $args);


//Roboty spawalnicze

    $labels = array(
        'name' => 'Roboty spawalnicze',
        'singular_name' => 'Robot spawalniczy',
        'menu_name' => 'Roboty spawalnicze',
        'add_new' => 'Dodaj nowego',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/roboty-spawalnicze/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'welding_robot_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('welding_robot', 'welding_robots'),
        'map_meta_cap' => true
    );
    register_post_type('welding_robot', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Roboty spawalnicze',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/roboty-spawalnicze',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('welding_robot_type', 'welding_robot', $args);


//Urządzenia i materiały kontrolno–pomiarowe

    $labels = array(
        'name' => 'Urządzenia i materiały kontrolno–pomiarowe',
        'singular_name' => 'Urządzenie i materiał kontrolno–pomiarowy',
        'menu_name' => 'Urządzenia i materiały kontrolno–pomiarowe',
        'add_new' => 'Dodaj nowe',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-i-materialy-kontrolno-pomiarowe/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'control_device_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('control_device', 'control_devices'),
        'map_meta_cap' => true
    );
    register_post_type('control_device', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Urządzenia i materiały kontrolno–pomiarowe',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/urzadzenia-i-materialy-kontrolno-pomiarowe',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('control_device_type', 'control_device', $args);


//Automatyczny magazyn

    $labels = array(
        'name' => 'Automatyczny magazyn',
        'singular_name' => 'Automatyczny magazyn',
        'menu_name' => 'Automatyczny magazyn',
        'add_new' => 'Dodaj nowy',
    );
    $args = array(
        'public' => true,
        'labels' => $labels,
        'rewrite' => array(
            'slug' => 'oferta/automatyczny-magazyn/produkt',
            'with_front' => false
        ),
        'taxonomy' => 'auto_warehouse_type',
        'has_archive' => true,
        'menu_icon' => 'dashicons-cart',
        'capability_type' => array('auto_warehouse', 'auto_warehouses'),
        'map_meta_cap' => true
    );
    register_post_type('auto_warehouse', $args);

    $args = array(
        'labels' => array(
            'name' => 'Podgałęzie - Automatyczny magazyn',
            'menu_name' => 'Podgałęzie'
        ),
        'hierarchical' => true,
        'rewrite' => array(
            'slug' => 'oferta/automatyczny-magazyn',
            'hierarchical' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'manage_figel_terms',
            'edit_terms' => 'manage_figel_terms',
            'delete_terms' => 'manage_figel_terms',
            'assign_terms' => 'assign_subbranch_type'
        )
    );
    register_taxonomy('auto_warehouse_type', 'auto_warehouse', $args);
}

function figel_columns_head($defaults)
{
    $pt = get_post_type();
    if ($pt != 'logotypes' && $pt != 'office' && $pt != 'product_manager' && $pt != 'news' && $pt != 'search-filter-widget') {
        return array(
            'cb' => '<input type="checkbox"/>',
            'title' => __('Title'),
            'subbranch' => __('Podgałąź'),
            'date' => __('Date')
        );
    } else {
        return $defaults;
    }
}

function figel_columns_content($column_name, $post_ID)
{
    if ($column_name == 'subbranch') {
        $terms = wp_get_post_terms($post_ID, get_post_type() . '_type');
        if ($terms) {
            $e_terms = '';
            foreach ($terms as $term) {
                $e_terms .= $term->name . '<br>';
            }
            echo trim($e_terms, '<br>');
        }
    }
}

function figel_taxonomy_columns()
{
    return array(
        'cb' => '<input type="checkbox"/>',
        'name' => __('Name'),
        'searchfilter' => __('Kod filtra'),
        'posts' => __('Liczba')
    );
}

function figel_taxonomy_columns_content($content, $column_name, $term_id)
{
    if ('searchfilter' == $column_name) {
        $sf = get_field('kod_filtra', $_REQUEST['taxonomy'] . '_' . $term_id);
        $sf_r = get_field('kod_wynikow_filtrowania', $_REQUEST['taxonomy'] . '_' . $term_id);
        $content = $sf && $sf_r ? $sf . '<br>' . $sf_r : 'Brak';
    }
    return $content;
}

//add_action('admin_init', 'figel_add_role_caps', 999);
add_action('after_switch_theme', 'figel_add_role_caps');
function figel_add_role_caps()
{

    $remove_roles = array(
        'editor',
        'author',
        'contributor',
        'subscriber'
    );

    foreach ($remove_roles as $role) {
        if (get_role($role)) {
            remove_role($role);
        }
    }

    get_role('administrator')->add_cap('manage_figel_terms');
    get_role('administrator')->add_cap('assign_news_type');
    get_role('administrator')->add_cap('assign_technology_type');

    //remove_role('product_manager');
    //remove_role('news_editor');

    if (!get_role('product_manager')) {

        add_role(
            'product_manager',
            'Menadżer produktów',
            array(
                'read' => true,
                'edit_posts' => false,
                'delete_posts' => false,
                'assign_subbranch_type' => true,
                'upload_files' => true,
                'edit_files' => true
            )
        );

        $product_cpts = array(
            array('welding_device', 'welding_devices'),
            array('welding_material', 'welding_materials'),
            array('welding_handle', 'welding_handles'),
            array('plasma_device', 'plasma_devices'),
            array('gas_welding_device', 'gas_welding_devices'),
            array('helping_device', 'helping_devices'),
            array('welding_chemical', 'welding_chemicals'),
            array('abrasive_material', 'abrasive_materials'),
            array('soldering_device', 'soldering_devices'),
            array('personal_protection', 'personal_protections'),
            array('dust_exhaust_device', 'dust_exhaust_devices'),
            array('welding_protection', 'welding_protections'),
            array('welding_automation', 'welding_automations'),
            array('welding_robot', 'welding_robots'),
            array('control_device', 'control_devices'),
            array('auto_warehouse', 'auto_warehouses')
        );

        $product_roles = array('product_manager', 'administrator');

        foreach ($product_roles as $the_role) {

            $role = get_role($the_role);

            $role->add_cap('read');

            foreach ($product_cpts as $cpt) {
                $role->add_cap('read_' . $cpt[0]);
                $role->add_cap('read_' . $cpt[1]);
                $role->add_cap('edit_' . $cpt[0]);
                $role->add_cap('edit_' . $cpt[1]);
                $role->add_cap('edit_others_' . $cpt[1]);
                $role->add_cap('edit_published_' . $cpt[1]);
                $role->add_cap('publish_' . $cpt[1]);
                $role->add_cap('delete_others_' . $cpt[1]);
                $role->add_cap('delete_private_' . $cpt[1]);
                $role->add_cap('delete_published_' . $cpt[1]);
            }
            $role->add_cap('assign_subbranch_type');
        }
    }

    if (!get_role('news_editor')) {

        add_role(
            'news_editor',
            'Edytor artykułów',
            array(
                'read' => true,
                'edit_posts' => false,
                'delete_posts' => false,
                'assign_news_type' => true,
                'assign_technology_type' => true,
            )
        );

        $article_cpts = array(
            array('news', 'newses'),
            array('article', 'articles'),
            array('technology', 'technologies')
        );

        $article_roles = array('news_editor', 'administrator');

        foreach ($article_roles as $the_role) {
            $role = get_role($the_role);
            $role->add_cap('read');

            foreach ($article_cpts as $cpt) {
                $role->add_cap('read_' . $cpt[0]);
                $role->add_cap('read_' . $cpt[1]);
                $role->add_cap('edit_' . $cpt[0]);
                $role->add_cap('edit_' . $cpt[1]);
                $role->add_cap('edit_others_' . $cpt[1]);
                $role->add_cap('edit_published_' . $cpt[1]);
                $role->add_cap('publish_' . $cpt[1]);
                $role->add_cap('delete_others_' . $cpt[1]);
                $role->add_cap('delete_private_' . $cpt[1]);
                $role->add_cap('delete_published_' . $cpt[1]);
            }
        }
    }
}