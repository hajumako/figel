<?php
/**
 * Created by PhpStorm.
 * User: Konrado
 * Date: 2016-09-08
 * Time: 09:17
 */

$con_material1_type = array(
    'stal węglowa' => 'stal węglowa',
    'stal niskostopowa' => 'stal niskostopowa',
    'stal wysokostopowa' => 'stal wysokostopowa',
    'stal duplex I superduplex' => 'stal duplex I superduplex',
    'stopy Al' => 'stopy Al',
    'stopy Ni, Cu' => 'stopy Ni, Cu',
    'żeliwo' => 'żeliwo',
    'tytan' => 'tytan'
);

$con_material2_type = array_merge(
    $con_material1_type,
    array(
        'taki sam, jak materiał 1' => 'taki sam, jak materiał 1'
    )
);

$welding_methods = array(
    '131' => '131 Spawanie elektrodą topliwą w osłonie gazu obojętnego (MIG)',
    '135' => '135 Spawania elektrodą topliwą w osłonie gazu aktywnego (MAG)',
    '136' => '136 Spawanie łukowe  drutem rdzeniowym w osłonie gazu aktywnego (FCAW)',
    '111' => '111 Spawanie łukowe elektrodą otuloną (MMA)',
    '141' => '141 Spawanie elektrodą nietopliwą w osłonie gazu obojętnego(TIG)',
    '121' => '121 Spawanie łukiem krytym pod topnikiem (SAW)'
);

$soldering_methods = array(
    'Zgrzewanie Punktowe' => 'Zgrzewanie Punktowe',
    'Zgrzewanie Liniowe' => 'Zgrzewanie Liniowe',
    'Zgrzewanie Garbowe' => 'Zgrzewanie Garbowe'
);

$cut_material_type = array(
    'stal węglowa' => 'stal węglowa',
    'stal stopowa' => 'stal stopowa',
    'stopy Al' => 'stopy Al',
    'stopy Ni, Cu' => 'stopy Ni, Cu',
    'żeliwo' => 'żeliwo',
);

function sendWeldingForm()
{
    global $welding_methods, $soldering_methods, $con_material1_type, $con_material2_type, $cut_material_type;
    $mail_to = MAIL_TO;
    $mail_from = MAIL_FROM;
    $subject = 'Zapytanie ofertowe';
    $headers = array(
        'From: Figel <' . $mail_from . '>',
    );

    $subject_client = 'Zgłoszenie przyjęte';
    $message_client = 'Dziękujemy za przesłanie zgłoszenia';

    if (isValid()) {
        $error = false;

        if ($_POST['process_type'] == '') {
            $error = true;
        } else {
            if (
                $_POST['process_type'] == 'con' &&
                (
                    $_POST['con_need'] == '' ||
                    $_POST['con_material1_type'] == '' ||
                    $_POST['con_material1_thickness'] == '' ||
                    $_POST['method_type'] == ''
                )
            )
                $error = true;
            elseif
            (
                $_POST['process_type'] == 'cut' &&
                (
                    $_POST['cut_need'] == '' ||
                    $_POST['cut_material_type'] == '' ||
                    $_POST['cut_material_thickness'] == ''
                )
            )
                $error = true;
        }

        if ($_POST['process_type'] == '') {
            $error = true;
        }

        if ($_POST['f_name'] == '') {
            $error = true;
        }
        if ($_POST['f_email'] == '') {
            $error = true;
        }
        if ($_POST['f_phone'] == '') {
            $error = true;
        }

        if (!$error) {
            $message = '
                <p>Osoba: ' . $_POST['f_name'] . '</p>
                <p>Firma: ' . $_POST['f_company'] . '</p>
                <p>Email: ' . $_POST['f_email'] . '</p>
                <p>Telefon: ' . $_POST['f_phone'] . '</p>';
            if($_POST['process_type'] == 'con') {
                $message .= '
                    <p>Proces: Łączenie materiałów' . '</p>
                    <p>Szukam: ' . ($_POST['con_need'] == 'device' ? 'Urządzeń' : 'Materiałów') . '</p>
                    <p>Gatunek materiału 1:<br>';
                foreach ($_POST['con_material1_type'] as $m) {
                    $message .= '<span style="margin-left:10px">' . $con_material1_type[$m] . '</span><br>';
                }
                $message .= '
                    </p>
                    <p>Grubość materiału 1: ' . $_POST['con_material1_thickness'] . 'mm';
                if ($_POST['con_material2_type'] != '') {
                    $message .= '<p>Gatunek materiału 2:<br>';
                    foreach ($_POST['con_material2_type'] as $m) {
                        $message .= '<span style="margin-left:10px">' . $con_material2_type[$m] . '</span><br>';
                    }
                    $message .= '
                        </p>
                        <p>Grubość materiału 1: ' . $_POST['con_material2_thickness'] . 'mm';
                }
                if ($_POST['method_type'] == 'welding') {
                    $message .= '<p>Preferowana metoda łączenia: Spawanie<br>';
                    foreach ($_POST['welding_method'] as $m) {
                        $message .= '<span style="margin-left:10px">' . $welding_methods[$m] . '</span><br>';
                    }
                    $message .= '</p>';
                } else if ($_POST['method_type'] == 'soldering') {
                    $message .= '<p>Preferowana metoda łączenia: Zgrzewanie';
                    foreach ($_POST['soldering_method'] as $m) {
                        $message .= '<span style="margin-left:10px">' . $soldering_methods[$m] . '</span><br>';
                    }
                    $message .= '</p>';
                } else {
                    $message .= '<p>Preferowana metoda łączenia: Nie wiem</p>';
                }
                $message .= '<p>Uwagi: ' . $_POST['additional_info'] . '</p>';
            } elseif ($_POST['process_type'] == 'cut') {
                $message .= '
                    <p>Proces: Cięcie</p>
                    <p>Szukam: ' . ($_POST['cut_need'] == 'device' ? 'Urządzeń' : 'Osprzętu') . '</p>
                    <p>Gatunek materiału ciętego:<br>';
                foreach ($_POST['cut_material_type'] as $m) {
                    $message .= '<span style="margin-left:10px">' . $cut_material_type[$m] . '</span><br>';
                }
                $message .= '
                    </p>
                    <p>Grubość materiału ciętego: ' . $_POST['cut_material_thickness'] . 'mm</p>
                    <p>Rodzaj cięcia: ' . $_POST['cut_method'] . '</p>
                    <p>Uwagi: ' . $_POST['cut_additional_info'] . '</p>';
            }
            
            $send1 = wp_mail(
                $mail_to,
                $subject,
                $message,
                $headers
            );

            $send2 = wp_mail(
                $_POST['f_email'],
                $subject_client,
                $message,
                $headers
            );

            if ($send1 && $send2)
                $result['type'] = 'success';
            else {
                $result['type'] = 'error';
                $result['messages'] = array('Nie udało się wysłać maila. Spróbuj ponownie później.');
                $result['code'] = 101;
            }

        } else {
            $result['type'] = 'error';
            $result['messages'] = array('Formularz zawiera błędy. Uzupełnij brakujące pola.');
            $result['code'] = 102;
        }
    } else {
        $result['type'] = 'error';
        $result['messages'] = array('Niepoprawna captcha.');
        $result['code'] = 103;
    }

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        $url = parse_url($_SERVER["HTTP_REFERER"]);
        header("Location: " . $url['scheme'] . '://' . $url['host'] . $url['path'] . '?result_type=' . $result['type'] . '&code=' . $result['code']);
    }

    die();
}

add_action('wp_ajax_send_welding_form', 'sendWeldingForm');
add_action('wp_ajax_nopriv_send_welding_form', 'sendWeldingForm');