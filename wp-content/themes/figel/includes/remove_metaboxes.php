<?php
/**
 * Created by PhpStorm.
 * User: Konrado
 * Date: 2016-09-06
 * Time: 10:26
 */

function figel_remove_meta_boxes() {
    $pt = get_current_screen()->post_type;
    if($pt != 'technology' && $pt != 'news')
        remove_meta_box( $pt .'_typediv', $pt, 'side' );
}
add_action( 'current_screen' , 'figel_remove_meta_boxes' );

function dashboard_remove_meta_boxes() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
}
add_action( 'admin_init' , 'dashboard_remove_meta_boxes' );