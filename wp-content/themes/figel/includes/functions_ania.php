<?php
// Hide admin bar
add_filter('show_admin_bar', '__return_false');

// **************************************
// REGISTER POLYLANG STRINGS TO TRANSLATE
// **************************************
if(function_exists('pll_register_string')) {
    pll_register_string('oferta', 'Oferta');
    pll_register_string('materialy-spawalnicze', 'Materiały spawalnicze');
    pll_register_string('urzadzenia-do-spawania-i-ciecia', 'Urządzenia do spawania i cięcia');
    pll_register_string('urzadzenia-do-zgrzewania', 'Urządzenia do zgrzewania');
    pll_register_string('osprzet-spawalniczy', 'Osprzęt spawalniczy');
    pll_register_string('bhp-wentylacja', 'BHP / Wentylacja');
    pll_register_string('chemia-spawalnicza', 'Chemia spawalnicza');
    pll_register_string('sprzet-pomocniczy', 'Sprzęt pomocniczy');
    pll_register_string('wpisz-szukane-wyrazenie', 'Wpisz szukane wyrażenie');
    pll_register_string('wyczysc', 'wyczyść');
}

function bxslider_scripts()
{
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style('bxslider-css', get_template_directory_uri() . '/css/libs/jquery.bxslider.css', array(), null);

    wp_enqueue_style('lightbox', get_template_directory_uri() . '/css/libs/lightbox.css', array(), true);

    wp_enqueue_style('fancySelect', get_template_directory_uri() . '/css/fancySelect.css', array(), true);

    wp_register_style('mmenu-style', get_stylesheet_directory_uri() . '/css/libs/jquery.mmenu.all.css');
    wp_enqueue_style('mmenu-style');

    wp_register_style('no-js', get_stylesheet_directory_uri() . '/css/no-js.css');
    wp_enqueue_style('no-js');

    wp_enqueue_script('bxlisder-js', get_template_directory_uri() . '/js/jquery.bxslider.js', array(), true);

    wp_enqueue_script('waypoint', get_template_directory_uri() . '/js/jquery.waypoints.js', array(), true);

    wp_enqueue_script('lightbox', get_template_directory_uri() . '/js/lightbox.js', array(), true);

    wp_enqueue_script('fancySelect', get_template_directory_uri() . '/js/fancySelect.js', array(), true);

    wp_register_script('mmenu', get_template_directory_uri() . '/js/jquery.mmenu.min.all.js', array(),'', true);
    wp_enqueue_script('mmenu');


    wp_enqueue_script('scripts', get_template_directory_uri() . '/js/scripts.js', array(), true);

    if(is_singular() && 'news' != get_post_type() && 'technology' != get_post_type() && 'job_offer' != get_post_type() && 'article' != get_post_type()){
        wp_enqueue_script('products-scripts', get_template_directory_uri() . '/js/products.js', array(), true);
    }
}

add_action('wp_enqueue_scripts', 'bxslider_scripts');

function the_date_custom($d = '', $before = '', $after = '', $echo = true)
{
    $the_date = $before . get_the_date($d) . $after;
    $the_date = apply_filters('the_date', $the_date, $d, $before, $after);
    echo $the_date;
}

// add image sizes
add_image_size('product-list',230, 160);
add_image_size('product-gallery', 300, 300);
add_image_size('product-gallery-thumbnails', 60, 60);
add_image_size('news-gallery', 850, 650);
add_image_size('news-gallery-thumbnails', 140, 100);
add_image_size('news-list', 212, 144);
add_image_size('logotypes-flex', 230 , 100);

function my_mce4_options($init) {
    $default_colours = '"000000", "Black",
                      "993300", "Burnt orange",
                      "333300", "Dark olive",
                      "003300", "Dark green",
                      "003366", "Dark azure",
                      "000080", "Navy Blue",
                      "333399", "Indigo",
                      "333333", "Very dark gray",
                      "800000", "Maroon",
                      "FF6600", "Orange",
                      "808000", "Olive",
                      "008000", "Green",
                      "008080", "Teal",
                      "0000FF", "Blue",
                      "666699", "Grayish blue",
                      "808080", "Gray",
                      "FF0000", "Red",
                      "FF9900", "Amber",
                      "99CC00", "Yellow green",
                      "339966", "Sea green",
                      "33CCCC", "Turquoise",
                      "3366FF", "Royal blue",
                      "800080", "Purple",
                      "999999", "Medium gray",
                      "FF00FF", "Magenta",
                      "FFCC00", "Gold",
                      "FFFF00", "Yellow",
                      "00FF00", "Lime",
                      "00FFFF", "Aqua",
                      "00CCFF", "Sky blue",
                      "993366", "Red violet",
                      "FFFFFF", "White",
                      "FF99CC", "Pink",
                      "FFCC99", "Peach",
                      "FFFF99", "Light yellow",
                      "CCFFCC", "Pale green",
                      "CCFFFF", "Pale cyan",
                      "99CCFF", "Light sky blue",
                      "CC99FF", "Plum"';
    $custom_colours =  '"169ce1", "custom blue",
                       "e21f23", "custom red"';

    // build colour grid default+custom colors
    $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';

    // enable 6th row for custom colours in grid
    $init['textcolor_rows'] = 6;

    return $init;
}

add_filter('tiny_mce_before_init', 'my_mce4_options');

register_nav_menus( array(
    'mmenu'   => 'Menu mobilne',

) );

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

//Lets add Open Graph Meta Info

function insert_fb_in_head() {
    global $post;
    if ( !is_singular()) //if it is not a post or a page
        return;

    echo '<meta property="og:title" content="' . get_the_title() . '"/>';
    echo '<meta property="og:type" content="article"/>';
    echo '<meta property="og:url" content="' . get_permalink() . '"/>';
    echo '<meta property="og:site_name" content="Figel"/>';
    if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
        //$default_image="http://example.com/image.jpg"; //replace this with a default image on your server or an image in your media library
        // echo '<meta property="og:image" content="' . $default_image . '"/>';
    }
    else{
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
        echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
    }
    echo "
";
}

//function insert_google_in_head() {
//    global $post;
//    if ( !is_singular()) //if it is not a post or a page
//        return;
//
//    echo '<meta itemscope itemtype="http://schema.org/Article"/>';
//    echo '<meta itemprop = "headline" content="Top 10 Most ..."/>';
//    echo '<meta itemprop="description" content="lalallaala"/>';
//    echo '<meta itemprop="image" content="http://i.sportyfitness.com/media/tough-trail-lrg.jpg"/>';
//    echo "
//";
//}
//add_action('wp_head', 'insert_google_in_head', 9999 );
add_action( 'wp_head', 'insert_fb_in_head', 9999 );

function custom_get_the_content($more_link_text = null, $strip_teaser = false) {
    $content = get_the_content( $more_link_text, $strip_teaser );

    /**
     * Filters the post content.
     *
     * @since 0.71
     *
     * @param string $content Content of the current post.
     */
    $content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );
    return $content;

}

function excerpt($limit) {

    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt)>=$limit) {

        array_pop($excerpt);

        $excerpt = implode(" ",$excerpt).' [...]';

    } else {

        $excerpt = implode(" ",$excerpt);

    }

    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

    return $excerpt;

};