<?php
/**
 * Created by PhpStorm.
 * User: Konrado
 * Date: 2016-09-08
 * Time: 09:17
 */

function sendCartForm()
{
    $mail_to = MAIL_TO;
    $mail_from = MAIL_FROM;
    $subject = 'Zapytanie ofertowe';
    $headers = array(
        'From: Figel <' . $mail_from . '>',
    );

    $subject_client = 'Zgłoszenie przyjęte';
    //$message_client = 'Dziękujemy za przesłanie zgłoszenia';

    $err_messages = array();

    if (isValid()) {
        $error = false;

        if ($_POST['f_name'] == '') {
            $error = true;
            array_push($err_messages, 'pole Imię i nazwisko jest puste');
        }
        if ($_POST['f_email'] == '') {
            $error = true;
            array_push($err_messages, 'pole Email jest puste');
        }
        if ($_POST['f_phone'] == '') {
            $error = true;
            array_push($err_messages, 'pole Telefon jest puste');
        }

        if (!$error) {
            $message =
                '<p>Osoba: ' . $_POST['f_name'] . '</p>' .
                '<p>Firma: ' . $_POST['f_company'] . '</p>' .
                '<p>Email: ' . $_POST['f_email'] . '</p>' .
                '<p>Telefon: ' . $_POST['f_phone'] . '</p>' .
                '<p>Uwagi: ' . $_POST['f_additional_info'] . '</p>';

            $message_client = $message . '<p>Dziękujemy za złożenie zapytania, przygotujemy ofertę na:</p>';

            foreach ($_POST['products'] as $pid) {
                $p = get_post($pid);
                $message .= '<p style="color:#f00; margin-bottom:20px;display:block">' . $p->post_title . '</p>';
                $message_client .= '<p style="color:#f00; margin-bottom:20px;display:block">' . $p->post_title . '</p>';
            }

            $send1 = wp_mail(
                $mail_to,
                $subject,
                $message,
                $headers
            );

            $send2 = wp_mail(
                $_POST['f_email'],
                $subject_client,
                $message_client,
                $headers
            );

            if ($send1 && $send2)
                $result['type'] = 'success';
            else {
                $result['type'] = 'error';
                $result['messages'] = array('nie udało się wysłać maila');
                $result['code'] = 101;
            }

        } else {
            $result['type'] = 'error';
            $result['messages'] = $err_messages;
            $result['code'] = 102;
        }
    } else {
        $result['type'] = 'error';
        $result['messages'] = array('niepoprawna captcha');
        $result['code'] = 103;
    }

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    }
    else {
        $url = parse_url($_SERVER["HTTP_REFERER"]);
        header("Location: " . $url['scheme'] . '://' . $url['host'] . $url['path'] . '?result_type=' . $result['type'] . '&code=' . $result['code']);
    }

    die();
}

add_action('wp_ajax_send_cart_form', 'sendCartForm');
add_action('wp_ajax_nopriv_send_cart_form', 'sendCartForm');

