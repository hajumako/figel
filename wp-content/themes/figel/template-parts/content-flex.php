<?php
/**
 * Flex content
 *
 *
 */
?>

<?php if (get_field('flex_content')): ?>
    <?php while (has_sub_field('flex_content')): ?>
        <?php if (get_row_layout() == 'tekst'): ?>
            <div class="text">
                <?php the_sub_field('tekst'); ?>
            </div>
        <?php elseif (get_row_layout() == 'tekst2kolumny'): ?>
            <div class="text-columns">
                <div class="left-col">
                    <?php the_sub_field('tekst-lewy'); ?>
                </div>
                <div class="right-col">
                    <?php the_sub_field('tekst-prawy'); ?>
                </div>
            </div>
        <?php elseif (get_row_layout() == 'zdjęcie'): ?>
            <div class="image">
                <?php $image = get_sub_field('zdjęcie'); ?>
                <a href="<?php echo $image['sizes']['large']; ?>" data-lightbox="image">
                    <img width="300" src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['title']; ?>"/>
                </a>
                <h6><?php the_sub_field('podpis'); ?></h6>
            </div>
        <?php elseif (get_row_layout() == 'szary_box'): ?>
            <div class="gray-box">
                <?php the_sub_field('tekst'); ?>
            </div>
        <?php elseif (get_row_layout() == 'cytat'): ?>
            <div class="quote">
                <?php the_sub_field('tekst'); ?>
            </div>
        <?php elseif (get_row_layout() == 'galeria'): ?>
            <div class="gallery">
                <ul class="bxslider-news" style="display: none;">
                    <?php foreach (get_sub_field('galeria') as $image) : ?>
                        <li>
                            <a href="<?php echo $image['sizes']['large']; ?>" data-lightbox="news">
                                <?php echo wp_get_attachment_image($image['id'], 'news-gallery'); ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <ul class="bx-pager-news" style="display: none;">
                    <?php for ($imageIndex = 0; $imageIndex < count(get_sub_field('galeria')); $imageIndex++): ?>
                        <li>
                            <a data-slideIndex="<?php echo $imageIndex; ?>" href="">
                                <?php echo wp_get_attachment_image(get_sub_field('galeria')[$imageIndex]['id'], 'news-gallery-thumbnails'); ?>
                            </a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </div>
        <?php elseif (get_row_layout() == 'menadżer_produktu'): ?>
            <div class="person">
                <?php $manager = get_post(get_sub_field('menadżer_produktu')); ?>
                <h3><?php echo $manager->post_title; ?></h3>
                <p class="stanowisko"><?php echo get_field('stanowisko', $manager->ID); ?></p>
                <div class="linki">
                    <?php if (get_field('email', $manager->ID)): ?>
                        <a href="mailto:<?php echo get_field('email', $manager->ID); ?>">
                            <span>E:</span> <?php echo get_field('email', $manager->ID); ?>
                        </a>
                    <?php endif; ?>
                    <?php foreach (get_field('nr_telefonow', $manager->ID) as $f) : ?>
                        <a href="tel:<?php echo $f['telefon']; ?>">
                            <span><?php echo $f['rodzaj_telefonu']; ?></span> <?php echo $f['telefon']; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php elseif (get_row_layout() == 'tabela'): ?>
            <div class="table">
                <?php if (get_sub_field('typ') == 'Z tytułem'): ?>
                    <h3><?php echo get_sub_field('tytuł'); ?></h3>
                <?php endif; ?>

                <table class="<?php echo get_sub_field('liczba_kolumn') == 1 ? 'single' : 'double'; ?>">
                    <tbody>
                    <?php foreach (get_sub_field('wiersze') as $row): ?>
                        <tr>
                            <td>
                                <?php echo $row['kolumna_1']; ?>
                            </td>
                            <?php if (get_sub_field('liczba_kolumn') == 2): ?>
                                <td>
                                    <?php echo $row['kolumna_2']; ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php elseif (get_row_layout() == 'załączniki'): ?>
            <div class="attachments">
                <p>Załączniki:</p>
                <ul>
                <?php foreach (get_sub_field('plik') as $file) : ?>
                    <li>
                        <a href="<?php echo $file['plik']['url']; ?>" target="_blank">
                            <?php echo $file['nazwa']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
        <?php elseif (get_row_layout() == 'lista_logotypow'): ?>
            <div class="logotypes">
                <?php foreach (get_sub_field('logotyp') as $logo) : ?>
                    <?php if($logo['link']) : ?>
                        <a href="<?php echo $logo['link']; ?>" target="_blank">
                    <?php else : ?>
                        <span>
                    <?php endif; ?>
                        <img src="<?php echo $logo['grafika']['sizes']['logotypes-flex']; ?>" title="<?php echo $logo['grafika']['title']; ?>"
                             alt="<?php echo $logo['grafika']['title']; ?>"/>
                        <?php echo $logo['podpis'] ? '<h5>' . $logo['podpis'] . '</h5>' : ''; ?>
                    <?php if($logo['link']) : ?>
                        </a>
                    <?php else : ?>
                        </span>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>
