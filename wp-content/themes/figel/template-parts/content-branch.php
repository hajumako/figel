<?php
/**
 * Lista podgałęzi danej gałęzi
 * 
 */
?>

<?php $terms = get_terms(array(
    'taxonomy' => get_field('typ_postu_podrzednego') . '_type',
    'hide_empty' => false,
    'parent' => 0,
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'meta_key' => 'branch-letter'
)); ?>
<?php if(count($terms)): ?>
    <div class="content-wrapper offer">
        <div class="offer-list">
        <h2><?php the_title(); ?></h2>
        <?php foreach($terms as $term): ?>
            <?php if(get_field('ukryta', get_field('typ_postu_podrzednego') . '_type' . '_' . $term->term_id) == null): ?>
            <div class="box">
                <a href="<?php echo get_term_link($term); ?>" >
                    <div class="image-wrapper">
                        <?php echo wp_get_attachment_image( get_field('thumbnail', get_field('typ_postu_podrzednego') . '_type' . '_' . $term->term_id), 'product-list' ); ?>
                    </div>
                    <h4><?php echo $term->name; ?></h4>
                </a>
            </div>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
<?php else : ?>
    <?php get_template_part( 'template-parts/content', 'products' ); ?>
<?php endif; ?>
