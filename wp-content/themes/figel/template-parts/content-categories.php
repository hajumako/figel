<?php
/**
 * Lista kategorii aktualnośći
 *
 */
?>

<div class="categories-wrapper">
    <h4>Kategorie</h4>
    <ul>
        <li>
            <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>">
                Wszystkie
            </a>
        </li>
        <?php foreach (get_terms(array('taxonomy' => 'news_type', 'hide_empty' => false, 'orderby' => 'description')) as $t) : ?>
            <li>
                <a href="<?php echo get_term_link($t); ?>">
                    <?php echo $t->name; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>