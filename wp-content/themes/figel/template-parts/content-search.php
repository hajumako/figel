<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php 
$ppp = get_option('posts_per_page');
?>

<?php if (have_posts()) : ?>
    <?php $wrapped_search = 0; ?>
    <div class="search-list">
    <?php while (have_posts()) : the_post(); ?>
        <?php if ($wrapped_search++ == 0): ?>
            <div class="row-search">
        <?php endif; ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <a href="<?php the_permalink(); ?>"><h2 class="title"><?php the_title(); ?></h2></a>
                <div>
                    <p><?php echo excerpt(20); ?></p>
                    <p class="date"><?php the_date_custom(); ?></p>
                    <a href="<?php the_permalink(); ?>" class="more">Więcej</a>
                </div>
            </header>
        </article>
        <?php if (($wrapped_search %= 2) == 0): ?>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
    <?php if ($wrapped_search % 2 != 0): ?>
        </div>
    <?php endif; ?>
    </div>
    <?php set_query_var('ppp', $ppp); ?>
    <?php set_query_var('paged', $paged ? $paged : 1); ?>
    <?php set_query_var('search', true); ?>
    <?php get_template_part('template-parts/content', 'pagination'); ?>
<?php else : ?>
    <?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>

