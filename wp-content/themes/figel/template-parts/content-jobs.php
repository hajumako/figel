<?php
/**
 * Lista ofert pract
 *
 */
?>

<?php
$ppp = 4;
$paged = $_GET['_page'] ? $_GET['_page'] : 1;
$args = array(
    'posts_per_page' => $ppp,
    'post_type' => 'job_offer',
    'orderby' => 'date',
    'order' => 'DESC',
    'tax_query' => array(
        array(
            'taxonomy' => 'language',
            'field' => 'slug',
            'terms' => pll_current_language('slug'),
        ),
    ),
    'paged' => $paged
);

$wp_query = new WP_Query($args); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <a href="<?php the_permalink(); ?>"><h4 class="title"><?php the_title(); ?></h4></a>
                <div class="jobs-list">
<!--                    --><?php //the_excerpt(); ?>
                    <p class="place">Miejsce pracy: <b><?php the_field('miejsce_pracy'); ?></b></p>
                    <p class="date">Data dodania: <b><?php the_date_custom(); ?></b></p>
                    <a href="<?php the_permalink(); ?>" class="more">Więcej</a>
                </div>
            </header>
        </article>
    <?php endwhile; ?>
    <?php set_query_var('ppp', $ppp); ?>
    <?php get_template_part('template-parts/content', 'pagination'); ?>
<?php else : ?>
    <?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>
