<?php
/**
 * Paginacja produktów w widoku bez filtracji
 *
 */
?>

<?php
$paged == 1 && $is_first = true;
$paged == $wp_query->max_num_pages && $is_last = true;
$base = $ppp;
$p = min(max($paged - floor($base / 2), 1), $wp_query->max_num_pages - $base + 1);
?>

<?php if ($wp_query->max_num_pages > 1) : ?>
    <div class="pagination">
        <!--        Strona --><?php //echo $paged; ?><!-- z --><?php //echo $wp_query->max_num_pages; ?><!--<br/>-->
        <div class="nav-first">
            <?php if (!$is_first) : ?>
                <?php if (isset($search) && $search) : ?>
                    <a href="<?php echo bloginfo('url'); ?>/?s=<?php echo $s; ?>"></a>
                <?php else : ?>
                    <a href="?_page=1"></a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="nav-prev">
            <?php if ($paged > 1) : ?>
                <?php if (isset($search) && $search) : ?>
                    <a href="<?php echo bloginfo('url'); ?>/page/<?php echo $paged - 1; ?>/?s=<?php echo $s; ?>"></a>
                <?php else : ?>
                    <a href="?_page=<?php echo $paged - 1; ?>"></a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php /*for ($i = max($start, 1); $i <= min($start + $base - 1, $wp_query->max_num_pages); $i++) : */ ?><!--
            <?php /*if ($i == $paged) : */ ?>
                <span class="active"><?php /*echo $i; */ ?></span>
            <?php /*else : */ ?>
                <a href="?_page=<?php /*echo $i; */ ?>"><?php /*echo $i; */ ?></a>
            <?php /*endif; */ ?>
        --><?php /*endfor; */ ?>
        <?php for ($i = 0; $i < $base; $i++) : ?>
            <?php if ($p > 0) : ?>
                <?php if ($p == $paged) : ?>
                    <span class="active"><?php echo $p; ?></span>
                <?php else : ?>
                    <?php if (isset($search) && $search) : ?>
                        <a href="<?php echo bloginfo('url'); ?>/page/<?php echo $p; ?>/?s=<?php echo $s; ?>"><?php echo $p; ?></a>
                    <?php else : ?>
                        <a href="?_page=<?php echo $p; ?>"><?php echo $p; ?></a>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php $p++; ?>
        <?php endfor; ?>
        <div class="nav-next">
            <?php if ($paged < $wp_query->max_num_pages) : ?>
                <?php if (isset($search) && $search) : ?>
                    <a href="<?php echo bloginfo('url'); ?>/page/<?php echo $paged + 1; ?>/?s=<?php echo $s; ?>"></a>
                <?php else : ?>
                    <a href="?_page=<?php echo $paged + 1; ?>"></a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="nav-last">
            <?php if (!$is_last) : ?>
                <?php if (isset($search) && $search) : ?>
                    <a href="<?php echo bloginfo('url'); ?>/page/<?php echo $wp_query->max_num_pages; ?>/?s=<?php echo $s; ?>"></a>
                <?php else : ?>
                    <a href="?_page=<?php echo $wp_query->max_num_pages; ?>"></a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
