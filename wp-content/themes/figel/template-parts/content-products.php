<?php
/**
 * Lista produktów
 *
 */

$page_filter_results = get_field('kod_wynikow_filtrowania');
$term_filter_results = get_field('kod_wynikow_filtrowania', get_queried_object()->taxonomy . '_' . get_queried_object()->term_id);
?>

<div class="content-wrapper offer products">
    <?php get_template_part('template-parts/content', 'filters'); ?>
    <div class="offer-list">

        <?php
        if (($page_filter_results != '' && $page_filter_results != false) || ($term_filter_results != '' && $term_filter_results != false)):
            if ($post->post_type == 'page') : ?>
                <h2><?php the_title(); ?></h2>
                <?php echo do_shortcode($page_filter_results); ?>
            <?php else : ?>
                <h2><?php echo get_queried_object()->name; ?></h2>
                <?php echo do_shortcode($term_filter_results); ?>
            <?php endif; ?>
        <?php else :
            $paged = $_GET['_page'] ? $_GET['_page'] : 1;
            $ppp = 12;
            if ($taxonomy) {
                echo '<h2>' . get_queried_object()->name . '</h2>';
                $args = array(
                    'posts_per_page' => 12,
                    'post_type' => str_replace('_type', '', $taxonomy),
                    'meta_key' => 'produkt_archiwalny',
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => $taxonomy,
                            'field' => 'slug',
                            'terms' => $term,
                        ),
                        array(
                            'taxonomy' => 'language',
                            'field' => 'slug',
                            'terms' => pll_current_language('slug'),
                        ),
                    ),
                    'paged' => $paged
                );
            } else {
                echo '<h2>' . get_the_title() . '</h2>';
                $args = array(
                    'posts_per_page' => $ppp,
                    'post_type' => get_field('typ_postu_podrzednego'),
                    'meta_key' => 'produkt_archiwalny',
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'language',
                            'field' => 'slug',
                            'terms' => pll_current_language('slug'),
                        ),
                    ),
                    'paged' => $paged
                );
            }

            $wp_query = new WP_Query($args);

            if (have_posts()) : ?>

                Znaleziono <?php echo $wp_query->found_posts; ?> pasujących produktów<br/>

                <?php while (have_posts()) : the_post(); ?>
                    <?php
                    if (get_field('produkt_archiwalny'))
                        $labels = array('box', 'archived');
                    else {
                        $labels = get_field('etykiety');
                        $labels = array('box', is_array($labels) ? $labels[0] : $labels);
                    }
                    ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class($labels); ?>>
                        <a href="<?php the_permalink(); ?>">
                            <div class="image-wrapper"><?php echo wp_get_attachment_image(get_field('zdjęcia')[0]['id'], 'thumbnail'); ?></div>
                            <h4><?php the_title(); ?></h4>
                        </a>
                        <button type="button" class="add_to_cart" onclick="addToCart(this)" data-id="<?php the_ID(); ?>"> + Zapytanie</button>
                    </article>
                <?php endwhile; ?>
                <?php set_query_var('ppp', $ppp); ?>
                <?php get_template_part('template-parts/content', 'pagination'); ?>

            <?php else : ?>
                <div class="not-found">
                    <div class="page-content">
                        <img src="<?php echo get_template_directory_uri().'/images/not-found-article.png'?>"
                        <h6>Nie znaleziono produktów o wybranych parametrach</h6>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>