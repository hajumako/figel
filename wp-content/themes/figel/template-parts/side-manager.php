<?php
/**
 * Menadżer przyporządkowany do strony/artykułu/aktualności
 *
 */
?>

<?php $manager = get_field('menadżer', $posts[0]->ID); ?>
<?php if ($manager): ?>
    <div class="person">
        <h3><?php echo $manager->post_title; ?></h3>
        <p class="stanowisko"><?php echo get_field('stanowisko', $manager->ID); ?></p>
        <div class="linki">
            <?php if (get_field('email', $manager->ID)): ?>
                <a href="mailto:<?php echo get_field('email', $manager->ID); ?>">
                    <span>E:</span> <?php echo get_field('email', $manager->ID); ?>
                </a>
            <?php endif; ?>
            <?php foreach (get_field('nr_telefonow', $manager->ID) as $f) : ?>
                <a href="tel:<?php echo $f['telefon']; ?>">
                    <span><?php echo $f['rodzaj_telefonu']; ?></span> <?php echo $f['telefon']; ?>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>