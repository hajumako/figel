<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php

$manager = get_product_manager($post);

$exclude_names = ['podgałąź', 'zdjęcia', 'do_pobrania', 'dane_techniczne', 'wideo', 'etykiety', 'produkt_archiwalny', 'cena', 'kolejność'];

foreach ($fields = get_fields() as $key => $value) {
    if (!in_array($key, $exclude_names)) {
        if($key == 'zakres_prądowy_min')
            $min = $value;
        elseif($key == 'zakres_prądowy_max')
            $max = $value;
        else
            $basic_parameters[$key] = $value;
    }
}
if(!empty($min) && !empty($max))
    $basic_parameters['zakres'] = $min . ' - ' . $max;
wp_reset_postdata();

$images = $fields['zdjęcia'];
$files = $fields['do_pobrania'];
$additional_parameters = $fields['dane_techniczne'];
$video = $fields['wideo'];
if($fields['produkt_archiwalny'])
    $labels = array('box', 'archived');
else {
    $labels = $fields['etykiety'];
    $labels = array('box', is_array($labels) ? $labels[0] : $labels);
}
?>

<div id="post-<?php the_ID(); ?>" <?php post_class($labels); ?> >

    <header class="entry-header content-product">
        <?php the_title('<h4 class="entry-title">', '</h4>'); ?>
        <div class="gallery">
            <?php if ($images) : ?>
                <ul class="bx-pager-product" style="display: none;">
                    <?php
                    for ($imageIndex = 0; $imageIndex < count($images); $imageIndex++): ?>
                        <li>
                            <a data-slideIndex="<?php echo $imageIndex; ?>" href=""><?php echo wp_get_attachment_image($images[$imageIndex]['id'], 'product-gallery-thumbnails'); ?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
                <ul class="bxslider-product" style="display: none;">
                    <?php foreach ($images as $image): ?>
                        <li>
                            <a href="<?php echo $image['sizes']['large']; ?>" data-lightbox="products">
                                <?php echo wp_get_attachment_image($image['id'], 'product-gallery'); ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <p><a href="<?php echo $image['sizes']['large']; ?>" data-lightbox="products">Kliknij, aby powiększyć</a></p>
            <?php endif; ?>
        </div>

        <div class="product-intro">
            <div class="col1">
                <?php if(get_field('product_brand', $post->ID)) : ?>
                    <div class="brand">
                        <h5>Producent:</h5>
                        <?php $brand = get_post_by_title(get_field('product_brand', $post->ID)['label'], 'logotypes'); ?>
                        <?php if(get_field('link', $brand->ID)) : ?>
                            <a href="<?php the_field('link', $brand->ID); ?>" target="_blank">
                                <?php echo get_the_post_thumbnail($brand->ID, 'news-gallery-thumbnails'); ?>
                            </a>
                        <?php else : ?>
                            <?php echo get_the_post_thumbnail($brand->ID, 'news-gallery-thumbnails'); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="manager">
                    <h5>Menadżer produktu:</h5>
                    <?php if ($manager): ?>
                        <p><span><?php echo $manager->post_title; ?></span></p><br/>
                        <?php if (get_field('adres_email', $manager->ID)): ?>
                            <p class="mail">
                                <a href="mailto:<?php echo get_field('adres_email', $manager->ID); ?>">
                                    <?php echo get_field('adres_email', $manager->ID); ?>
                                </a>
                            </p>
                        <?php endif; ?>
                        <?php if (get_field('nr_telefonow', $manager->ID)): ?>
                            <p class="tel">
                                <?php foreach (get_field('nr_telefonow', $manager->ID) as $f) : ?>
                                    <a href="tel:<?php echo $f['telefon']; ?>">
                                        <span><?php echo $f['rodzaj_telefonu']; ?></span> <?php echo $f['telefon']; ?>
                                    </a>
                                <?php endforeach; ?>
                            </p>
                        <?php endif; ?>
                    <?php else : ?>
                        <p><span>Brak</span></p>
                    <?php endif; ?>
                </div>
                <?php if((in_array('sale', $labels) || in_array('promo', $labels)) && $fields['cena'] != ''): ?>
                    <h5 class="price">Cena: <span><?php echo $fields['cena']; ?></span></h5>
                <?php endif; ?>
                <button type="button" class="add_to_cart" onclick="addToCart(this)" data-id="<?php the_ID(); ?>">+ Zapytanie</button>
            </div>
            <?php if ($files) : ?>
                <div class="attachments">
                    <h5>Do pobrania:</h5>
                    <ul>
                        <?php foreach ($files as $f) : ?>
                            <li>
                                <a href="<?php echo $f['plik']['url']; ?>" target="_blank">
                                    <?php echo $f['plik']['title']; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </header><!-- .entry-header -->

    <div class="tabbed-content">

        <?php

        if ($post->post_content) {
            $lis .= '<li class="tab" data-tab="opis">Opis</li>';
            $divs .= '<div id="opis">' . custom_get_the_content() . '</div>';
        }
        if ($basic_parameters || $additional_parameters) {
            $lis .= '<li class="tab" data-tab="dane">Dane techniczne</li>';
            $divs .= '<div id="dane">
                        <div class="dane-wrapper">';
            if ($basic_parameters) {
                foreach ($basic_parameters as $field_name => $p) {
                    if($p != '') {
                        if($field_name == 'zakres') {
                            $divs .= '
                            <div class="par">
                                <p class="par-name">Zakres prądowy [A]</p>
                                <p class="par-value">' . $p . '</p>
                            </div>';
                        } else {
                            $field = get_field_object($field_name);
                            $divs .= '
                            <div class="par">
                                <p class="par-name">' . $field['label'] . '</p>
                                <p class="par-value">' . (is_object($p) ? $p->post_title : (is_array($p) ? $p['label'] : $p)) . '</p>
                            </div>';
                        }
                    }
                }
            }
            if ($additional_parameters) {
                foreach ($additional_parameters as $p) {
                    $divs .= '
				        <div class="par">
				            <p class="par-name' . ($p['indented'] ? ' indent' : '') . '">' . $p['nazwa_pola'] . '</p>
				            <p class="par-value">' . $p['wartość'] . '</p>
                        </div>';
                }
            }
            $divs .= '</div>
                    </div>';
        }
        if ($video) {
            $lis .= '<li class="tab" data-tab="wideo">Wideo</li>';
            $divs .= '<div id="wideo">' . $video . '</div>';
        }
        ?>

        <ul>
            <?php echo $lis; ?>
        </ul>
        <?php echo $divs; ?>

    </div><!-- .entry-content -->
</div><!-- #post-## -->
