<?php
/**
 * Ostatnie aktualności/artykuły/technologie
 *
 */
?>

<?php $recent = get_posts(
    array(
        'post_type' => $posts[0]->post_type,
        'posts_per_page' => 2,
        'post__not_in' => array($posts[0]->ID),
        'orderby' => 'date',
        'order' => 'DESC'
    ));
?>
<div class="recent-wrapper">
    <?php if (count($recent)): ?>
        <ul>
            <?php foreach ($recent as $r): ?>
                <li>
                    <a class="title" href="<?php the_permalink($r->ID); ?>">
                        <?php echo $r->post_title; ?>
                    </a>
                    <p class="date"><?php echo mysql2date('j F Y', $r->post_date); ?></p>
                    <p class="excerpt"><?php echo $r->post_excerpt; ?></p>
                    <a class="more" href="<?php echo get_permalink($r->ID); ?>">
                        Więcej
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <?php get_template_part('template-parts/side', 'manager'); ?>
</div>