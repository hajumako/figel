<?php
/**
 * Mapa z zaznaczonymi oddziałami
 *
 */
?>

<div id="map-wrapper">
<!--    <script type="text/javascript" src="--><?php //echo bloginfo('url'); ?><!--/wp-content/themes/figel/js/map.js"></script>-->
<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJIFs6NuTWNG4fPmW6OUs-5X7bU9e7Y8c&amp;callback=initMap&amp;ver=20160512" async-->
<!--            defer></script>-->
    <style>
        #map {
            height: 300px;
        }
    </style>
    <div id="map"></div>

    <?php 
    $offices = get_posts(array(
        'post_type' => 'office',
        'posts_per_page' => -1
    ));
    ?>

    <?php if ($offices) : ?>
        <?php $locations = '['; ?>
        <?php foreach ($offices as $i => $office): ?>
            <?php foreach (get_fields($office->ID) as $name => $field): ?>
                <?php
                if ($name == 'wspołrzędne1') :
                    $locations .= '[' . $field . ',';
                elseif ($name == 'wspołrzędne2') :
                    $locations .= $field . ',';
                    ?>
                <?php elseif ($name == 'adres'): ?>
                    <?php $locations .= json_encode($field) . '],'; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php $locations .= ']'; ?>
        <?php echo '<script>var locations = ' . $locations . '</script>'; ?>
    <?php endif; ?>
</div>
