<?php
/**
 *
 */
?>

<nav class="breadcrumbs">
    <?php
    if (get_queried_object()->term_id) {
        $url = get_term_link(get_queried_object()->term_id);
        $page_title = get_queried_object()->name;
    } elseif($post->post_type == 'post') {
        $page = get_post(get_option('page_for_posts'));
        $url = get_permalink($page);
        $page_title = $page->post_title;
    } else {
        $url = get_permalink();
        $page_title = $post->post_title;
    }
    $home = get_home_url();
    $url_rpl = str_replace($home, 'home_url', $url);
    $url_arr = explode('/', trim($url_rpl, '/'));
    $url_arr[0] = $home;
    $bc_url = '';
    for ($i = 0; $i < count($url_arr) - 1; $i++) : ?>
        <?php
        if ($url_arr[$i] == 'produkt') {
            $terms = wp_get_post_terms($post->ID, $post->post_type . '_type');
            $term = $terms[0]->parent ? get_term($terms[0]->parent) : $terms[0];
            if($term) {
                $temp_url = $bc_url . $term->slug . '/';
                $title = $term->name;
            } else {
                break;
            }
        } elseif ($url_arr[$i] == 'aktualnosc' || $url_arr[$i] == 'aktualnosci') {
            $post = get_post(get_option('page_for_posts'));
            $temp_url = $bc_url . $post->post_name . '/';
            $title = $post->post_title;
        } elseif ($url_arr[$i] == 'artykul') {
            $post = get_posts(array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'templates-pages/articles.php'
            ))[0];
            $temp_url = $bc_url . $post->post_name . '/';
            $title = $post->post_title;
        } elseif ($url_arr[$i] == 'oferta-pracy') {
            $post = get_posts(array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'templates-pages/jobs.php'
            ))[0];
            $temp_url = $bc_url . $post->post_name . '/';
            $title = $post->post_title;
        } elseif ($url_arr[$i] == 'technologia-spawania') {
            $post = get_posts(array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'templates-pages/technologies.php'
            ))[0];
            $temp_url = $bc_url . $post->post_name . '/';
            $title = $post->post_title;
        } else {
            $temp_url = $bc_url . $url_arr[$i] . '/';
            $id = url_to_postid($temp_url);
            if($id == 0 && ($post->post_type == 'post' || $post->post_type == 'news'))
                $id = get_option('page_for_posts');
            $title = get_post($id)->post_title;
        }
        ?>
        <a href="<?php echo $temp_url; ?>"><?php echo $title; ?></a><span class="separator"></span>
        <?php $bc_url .= $url_arr[$i] . '/'; ?>
    <?php endfor; ?>
    <span><?php echo $page_title; ?></span>
</nav>