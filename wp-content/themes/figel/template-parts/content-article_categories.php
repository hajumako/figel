<?php
/**
 * Lista kategorii aktualnośći
 *
 */
?>

<?php
$pages = get_posts(array(
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
	'meta_value' => 'templates-pages/articles.php'
));
?>

<div class="categories-wrapper">
    <h4>Kategorie</h4>
    <ul>
        <li>
            <a href="<?php echo get_permalink($pages[0]->ID); ?>">
                Wszystkie
            </a>
        </li>
        <?php foreach (get_terms(array('taxonomy' => 'article_type', 'hide_empty' => false)) as $t) : ?>
            <li>
                <a href="<?php echo get_term_link($t); ?>">
                    <?php echo $t->name; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>