<?php
/**
 * Lista filtrów
 *
 */
$page_filter = get_field('kod_filtra');
$term_filter = get_field('kod_filtra', get_queried_object()->taxonomy . '_' . get_queried_object()->term_id); ?>

<?php if (($page_filter !== '' && $page_filter != false) || ($term_filter !== '' && $term_filter != false)): ?>
    <script>
        jQuery(document).ready(function () {
            $ = jQuery;
            if (!$('.sf-input-select').attr('data-combobox'))
                $('.sf-input-select').fancySelect();

            var observeDOM = (function () {
                var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
                    eventListenerSupported = window.addEventListener;

                return function (obj, callback) {
                    if (MutationObserver) {
                        var obs = new MutationObserver(function (mutations, observer) {
                            if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
                                callback();
                        });
                        obs.observe(obj, {childList: true, subtree: true});
                    }
                    else if (eventListenerSupported) {
                        obj.addEventListener('DOMNodeInserted', callback, false);
                        obj.addEventListener('DOMNodeRemoved', callback, false);
                    }
                }
            })();

            function hideEmpty() {
                $('form.searchandfilter ul li ul').each(function () {
                    $this = $(this);
                    if ($this.children('li').length <= 1)
                        $this.parent().addClass('hide-input');
                    else
                        $this.parent().removeClass('hide-input');
                });
            }

            hideEmpty();
            observeDOM(document.getElementsByClassName('searchandfilter')[0], hideEmpty);
        });
    </script>
    <div class="filters-wrapper">
        <h4>Filtrowanie</h4>
        <?php if ($post->post_type == 'page')
            echo do_shortcode($page_filter);
        else
            echo do_shortcode($term_filter);
        ?>
    </div>
<?php endif; ?>