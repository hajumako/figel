<?php
/**
 * Lista artykułów
 *
 */
?>

<?php
$ppp = 4;
$paged = $_GET['_page'] ? $_GET['_page'] : 1;
$args = array(
    'posts_per_page' => $ppp,
    'post_type' => 'article',
    'orderby' => 'date',
    'order' => 'DESC',
    'tax_query' => array(
        array(
            'taxonomy' => 'language',
            'field' => 'slug',
            'terms' => pll_current_language('slug'),
        ),
    ),
    'paged' => $paged
);

$wp_query = new WP_Query($args); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <a href="<?php the_permalink(); ?>"><h2 class="title"><?php the_title(); ?></h2></a>
                <div><?php the_excerpt(); ?>
                    <p class="date"><?php the_date_custom(); ?></p>
<!--                    --><?php //foreach (wp_get_post_terms(get_the_ID(), 'article_type') as $term) : ?>
<!--                        <h5 class="--><?php //echo $term->slug; ?><!--">--><?php //echo $term->name; ?><!--</h5>-->
<!--                    --><?php //endforeach; ?>
                    <a href="<?php the_permalink(); ?>" class="more">Więcej</a>
                </div>
            </header>
        </article>
    <?php endwhile; ?>
    <?php set_query_var('ppp', $ppp); ?>
    <?php get_template_part('template-parts/content', 'pagination'); ?>
<?php else : ?>
    <?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>
