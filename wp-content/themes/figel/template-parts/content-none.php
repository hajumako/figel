<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<section class="no-results not-found">
	<div class="page-content">
		<?php if ( is_search() ) : ?>
			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentysixteen' ); ?></p>

		<?php else : ?>
			<img src="<?php echo get_template_directory_uri().'/images/not-found-article.png'?>"
			<p> Nie znaleziono artykułu w wybranej kategorii.</p>
		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
