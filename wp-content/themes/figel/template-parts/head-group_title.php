<?php
/**
 *
 */
?>

<header class="page-header">
    <div clas="page-title-wrapper">
        <h1 class="page-title"><?php single_cat_title(); ?></h1>
    </div>

    <?php get_template_part('template-parts/head', 'breadcrumbs'); ?>
</header>