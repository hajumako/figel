<?php
/**
 *
 */
?>

<?php
$parent = array_reverse(get_post_ancestors($post->ID));

if ($post->post_type == 'post' || $post->post_type == 'news') {
    $first_parent = get_post(get_option('page_for_posts'));
} elseif ($taxonomy == 'article_type' || $post->post_type == 'article') {
    $parent = get_posts(array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'templates-pages/articles.php'
    ))[0];
    $first_parent = get_post(array_reverse(get_post_ancestors($parent->ID))[0]);
} elseif ($taxonomy == 'technology_type' || $post->post_type == 'technology') {
    $parent = get_posts(array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'templates-pages/technologies.php'
    ))[0];
    $first_parent = get_post(array_reverse(get_post_ancestors($parent->ID))[0]);
} elseif ($taxonomy == 'news_type') {
    $first_parent = get_post(get_option('page_for_posts'));
} elseif (count($parent) > 0) {
    $first_parent = get_post($parent[0]);
} elseif ($post->post_type == 'page') {
    $first_parent = $post;
} elseif (get_queried_object()->taxonomy) {
    $p = get_posts(array(
        'posts_per_page' => -1,
        'post_type' => 'page',
        'meta_key' => 'typ_postu_podrzednego',
        'meta_value' => str_replace('_type', '', get_queried_object()->taxonomy)
    ));
    $parent = array_reverse(get_post_ancestors($p[0]->ID));
    $first_parent = get_post($parent[0]);
} else {
    $p = get_posts(array(
        'posts_per_page' => -1,
        'post_type' => 'page',
        'meta_key' => 'typ_postu_podrzednego',
        'meta_value' => $post->post_type
    ));
    $parent = array_reverse(get_post_ancestors($p[0]->ID));
    $first_parent = get_post($parent[0]);
}
?>

<header class="page-header">
    <div class="page-title-wrapper">
        <h1 class="page-title"><?php echo $first_parent->post_title;; ?></h1>
    </div>

    <?php get_template_part('template-parts/head', 'breadcrumbs'); ?>
</header>
<!-- .page-header -->