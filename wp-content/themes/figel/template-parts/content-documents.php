<?php
/**
 * Created by PhpStorm.
 * User: Konrado
 * Date: 2016-09-09
 * Time: 09:23
 */
?>

<?php if (get_field('dokumenty')): ?>
    <div class="certificates">
        <?php foreach (get_field('dokumenty') as $doc): ?>
            <div class="box">
                <a <?php echo $doc['typ'] == 'Zdjęcie' ?
                    'href="' . $doc['zdjęcie']['sizes']['large'] . '" data-lightbox="logotype"' :
                    'href="' . $doc['plik']['url'] . '" target="_blank"'; ?>
                >
                    <div class="image-wrapper">
                        <img src="<?php echo $doc['grafika']['sizes']['product-list']; ?>" title="<?php echo $doc['grafika']['title']; ?>"
                             alt="<?php echo $doc['grafika']['title']; ?>"/>
                    </div>
                    <?php echo $doc['podpis'] ? '<h5>' . $doc['podpis'] . '</h5>' : ''; ?>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>



