<?php
/**
 * Przegląd technologii
 *
 */
?>
<div class="technologies">
<?php
$terms = get_terms(
    array(
        'taxonomy' => 'technology_type',
        'hide_empty' => false,
        'parent' => 0,
        'orderby' => 'term_id',
        'order' => 'ASC'
    ));
?>
<?php if ($terms): ?>
    <?php foreach ($terms as $term): ?>
        <h3 class="term"><?php echo $term->name; ?></h3>
        <?php
        $subterms = get_terms(
            array(
                'taxonomy' => 'technology_type',
                'hide_empty' => false,
                'parent' => $term->term_id,
                'orderby' => 'term_id',
                'order' => 'ASC'
            ));
        ?>
        <?php if ($subterms): ?>

            <?php foreach ($subterms as $subterm): ?>
                <div class="subterms-wrapper">
                <h4 class="subterm"><?php echo $subterm->name; ?></h4>
                <?php
                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'technology',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'technology_type',
                            'field' => 'term_id',
                            'terms' => $subterm->term_id,
                        ),
                        array(
                            'taxonomy' => 'language',
                            'field' => 'slug',
                            'terms' => pll_current_language('slug'),
                        ),
                    )
                );
                $query = new WP_Query($args);
                ?>
                <?php if ($query->have_posts()) : ?>
                    <ul>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <?php $redirect = get_field('redirect'); ?>
                        <li><a href="<?php echo $redirect ? $redirect : get_the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <?php
            $args = array(
                'posts_per_page' => -1,
                'post_type' => 'technology',
                'orderby' => 'date',
                'order' => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'technology_type',
                        'field' => 'term_id',
                        'terms' => $term->term_id,
                    ),
                    array(
                        'taxonomy' => 'language',
                        'field' => 'slug',
                        'terms' => pll_current_language('slug'),
                    ),
                )
            );
            $query = new WP_Query($args); 
            ?>
            <?php if ($query->have_posts()) : ?>
                <ul>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $redirect = get_field('redirect'); ?>
                    <li><a href="<?php echo $redirect ? $redirect : get_the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
</div>