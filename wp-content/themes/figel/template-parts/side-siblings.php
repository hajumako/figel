<?php
/**
 * Lista podstron strony nadrzędnej
 *
 */
?>

<?php
if ($post->post_parent) {
    $children = wp_list_pages("title_li=&child_of=" . $post->post_parent . "&echo=0&sort_column=menu_order");
} else {
    $children = wp_list_pages("title_li=&child_of=" . $post->ID . "&echo=0&sort_column=menu_order");
} ?>
<?php if ($children): ?>
<div class="sidebar-menu">
        <ul>
            <?php echo $children; ?></php>
        </ul>
    <?php get_template_part('template-parts/side', 'manager'); ?>
</div>
<?php endif; ?>