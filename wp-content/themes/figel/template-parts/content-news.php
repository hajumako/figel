<?php
/**
 * Lista aktualności
 *
 */
?>

<?php get_template_part('template-parts/content', 'categories'); ?>

<?php
$ppp = 4;
$paged = $_GET['_page'] ? $_GET['_page'] : 1;
if ($taxonomy == 'news_type')
    $args = array(
        'posts_per_page' => $ppp,
        'post_type' => 'news',
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'field' => 'slug',
                'terms' => $term,
            ),
            array(
                'taxonomy' => 'language',
                'field' => 'slug',
                'terms' => pll_current_language('slug'),
            ),
        ),
        'paged' => $paged
    );
else
    $args = array(
        'posts_per_page' => $ppp,
        'post_type' => 'news',
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => 'language',
                'field' => 'slug',
                'terms' => pll_current_language('slug'),
            ),
        ),
        'paged' => $paged
    );

$wp_query = new WP_Query($args); ?>
<div class="content-wrapper news-list">
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-header">
                    <a href="<?php the_permalink(); ?>"><h2 class = "title"><?php the_title(); ?></h2></a>
                    <a href="<?php the_permalink(); ?>">
                        <?php if(has_post_thumbnail()) : ?>
                            <div class="thumb-wrapper"><?php the_post_thumbnail('news-list'); ?></div>
                        <?php else : ?>
                        <div class="thumb-wrapper"><img src="<?php echo get_template_directory_uri();?>/images/logo-news.png" alt="empty"/></div>
                        <?php endif; ?>
                    </a>
                    <div><?php the_excerpt(); ?>
                        <div>
                            <p class="date"><?php the_date_custom(); ?></p>
                            <?php foreach(wp_get_post_terms(get_the_ID(), 'news_type') as $term) : ?>
                                <h5 class="etykieta <?php echo $term->slug; ?>"><?php echo $term->name; ?></h5>
                            <?php endforeach; ?>
                            <a href="<?php the_permalink(); ?>" class="more">Więcej</a>
                        </div>
                    </div>
                </header>
            </article>
        <?php endwhile; ?>
        <?php set_query_var('ppp', $ppp); ?>
        <?php get_template_part('template-parts/content', 'pagination'); ?>
    <?php else : ?>
        <?php get_template_part('template-parts/content', 'none'); ?>
    <?php endif; ?>
</div>
