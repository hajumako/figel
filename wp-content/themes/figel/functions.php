<?php

// Core
remove_action( 'wp_version_check', 'wp_version_check' );
remove_action( 'admin_init', '_maybe_update_core' );
add_filter( 'pre_transient_update_core', '__return_zero' );
add_filter( 'pre_site_transient_update_core', '__return_zero' );
add_filter('pre_option_update_core','__return_null');

// Plugins
remove_action( 'load-plugins.php', 'wp_update_plugins' );
remove_action( 'load-update.php', 'wp_update_plugins' );
remove_action( 'load-update-core.php', 'wp_update_plugins' );
remove_action( 'admin_init', '_maybe_update_plugins' );
remove_action( 'wp_update_plugins', 'wp_update_plugins' );
add_filter( 'pre_transient_update_plugins', '__return_zero' );
add_filter( 'pre_site_transient_update_plugins', '__return_zero' );
add_filter( 'site_transient_update_plugins', '__return_false' );

remove_action( 'load-themes.php', 'wp_update_themes' );
remove_action( 'load-update.php', 'wp_update_themes' );
remove_action( 'load-update-core.php', 'wp_update_themes' );
remove_action( 'admin_init', '_maybe_update_themes' );
remove_action( 'wp_update_themes', 'wp_update_themes' );
add_filter( 'pre_transient_update_themes', '__return_zero' );
add_filter( 'pre_site_transient_update_themes', '__return_zero' );


function stop_heartbeat() {
    wp_deregister_script('heartbeat');
}
add_action( 'init', 'stop_heartbeat', 1 );

/**
 * Disable Posts' meta from being preloaded
 * This fixes memory problems in the WordPress Admin
 */
// Only do this for admin
if ( is_admin() ) {
    function quellio_pre_get_posts(WP_Query $wp_query)
    {
        if (in_array(
            $wp_query->get('post_type'),
            array(
                'news',
                'article',
                'technology',
                'job_offer',
                'product_manager',
                'logotypes',
                'office',
                'employee',
                'welding_device',
                'welding_material',
                'welding_handle',
                'plasma_device',
                'gas_welding_device',
                'helping_device',
                'welding_chemical',
                'abrasive_material',
                'soldering_device',
                'personal_protection',
                'dust_exhaust_device',
                'welding_protection',
                'welding_automation',
                'welding_robot',
                'control_device',
                'auto_warehouse',
                'search-filter-widget'
            )
        )) {
            $wp_query->set('update_post_meta_cache', false);
        }
    }
    add_action('pre_get_posts', 'quellio_pre_get_posts');
}

if (version_compare($GLOBALS['wp_version'], '4.4-alpha', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
}

add_filter( 'post_row_actions', 'figel_disable_quick_edit', 10, 2 );
add_filter( 'page_row_actions', 'figel_disable_quick_edit', 10, 2 );

function figel_disable_quick_edit( $actions = array(), $post = null ) {

    // Remove the Quick Edit link
    if ( isset( $actions['inline hide-if-no-js'] ) ) {
        unset( $actions['inline hide-if-no-js'] );
    }

    // Return the set of links without Quick Edit
    return $actions;
}

if (!function_exists('quellio_theme_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * Create your own twentysixteen_setup() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     */
    function quellio_theme_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Twenty Sixteen, use a find and replace
         * to change 'twentysixteen' to the name of your theme in all the template files
         */
        load_theme_textdomain('twentysixteen', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for custom logo.
         *
         *  @since Twenty Sixteen 1.2
         */
        add_theme_support('custom-logo', array(
            'height' => 240,
            'width' => 240,
            'flex-height' => true,
        ));

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'top_main' => 'Menu główne',
            'lang' => 'Wybór języków',
            'social' => 'Media społecznościowe',
            'bottom' => 'Menu w stopce'
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ));

        // Indicate widget sidebars can use selective refresh in the Customizer.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif; // twentysixteen_setup
add_action('after_setup_theme', 'quellio_theme_setup');

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init()
{
    register_sidebar(array(
        'name' => 'Header',
        'id' => 'top-widget',
        'description' => __('Add widgets here to appear in your sidebar.', 'twentysixteen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'twentysixteen_widgets_init');

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function quellio_theme_content_width()
{
    $GLOBALS['content_width'] = apply_filters('quellio_theme_content_width', 840);
}

add_action('after_setup_theme', 'quellio_theme_content_width', 0);

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function quellio_theme_javascript_detection()
{
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action('wp_head', 'quellio_theme_javascript_detection', 0);

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function figel_scripts()
{
    // Reset stylesheet
    wp_enqueue_style('normalize', get_template_directory_uri(). '/css/libs/normalize.css', array());

    // Theme stylesheet.
    wp_enqueue_style('figel-style', get_template_directory_uri() . '/css/basic.css', array(), '0.1');

    if (is_singular() && wp_attachment_is_image()) {
        wp_enqueue_script('twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array('jquery'), '20160412');
    }

    wp_localize_script('twentysixteen-script', 'screenReaderText', array(
        'expand' => __('expand child menu', 'twentysixteen'),
        'collapse' => __('collapse child menu', 'twentysixteen'),
    ));

    if ((is_single() && get_post_type() != 'news') || get_the_ID() == 362 || wp_get_post_parent_id(get_the_ID()) == 11 || is_tax())
        wp_enqueue_script('cart-script', get_template_directory_uri() . '/js/cart.js', array('jquery'), '20160512', true);

    if( is_page_template( 'templates-pages/cart_form.php' )) {
        wp_enqueue_style('form_page', get_template_directory_uri(). '/css/form_page.css', array());
        wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js', null, '20160512', true);
        wp_enqueue_script('cart-validation', get_template_directory_uri() . '/js/validate_cart_form.js', array('jquery'), '20160512', true);
    }

    if( is_page_template( 'templates-pages/welding_form.php' ) ) {
        wp_enqueue_style('form_page', get_template_directory_uri(). '/css/form_page.css', array());
        wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js', null, '20160512', true);
        wp_enqueue_script('form-validation', get_template_directory_uri() . '/js/validate_welding_form.js', array('jquery'), '20160513', true);
    }

    if ( is_page_template( 'templates-pages/contact_main.php' ) || is_page_template( 'templates-pages/contact_subpage.php' )) {
        wp_enqueue_script('map-script', get_template_directory_uri() . '/js/map.js', array('jquery'), '20160512', true);
        wp_enqueue_script('gmaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBJIFs6NuTWNG4fPmW6OUs-5X7bU9e7Y8c&callback=initMap', array('jquery'), '20160512', true);

        $translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
        wp_localize_script( 'map-script', 'object_name', $translation_array );
    }
}
add_action('wp_enqueue_scripts', 'figel_scripts');

function add_async_attribute($tag, $handle) {
    if ( 'gmaps' !== $handle )
        return $tag;
    return str_replace(' src', ' async defer src', $tag);
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

function load_custom_wp_admin_scripts($hook)
{
    wp_register_style('custom_wp_admin_css', get_template_directory_uri() . '/admin/css/style.css', false, '1.0.0');
    wp_enqueue_style('custom_wp_admin_css');

    if ($hook == 'toplevel_page_branches_management') {
        wp_register_style('wp_admin_branches_manager_css', get_template_directory_uri() . '/admin/css/branches-manager.css', false, '1.0.0');
        wp_enqueue_style('wp_admin_branches_manager_css');

        wp_enqueue_script('wp_admin_branches_manager_js', get_template_directory_uri() . '/admin/js/branches-manager.js', array(), '1.0.0');
    }
}
add_action('admin_enqueue_scripts', 'load_custom_wp_admin_scripts');

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array $size Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr($sizes, $size)
{
    $width = $size[0];

    840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

    if ('page' === get_post_type()) {
        840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
    } else {
        840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
        600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
    }

    return $sizes;
}
add_filter('wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2);

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function figel_post_thumbnail_sizes_attr($attr, $attachment, $size)
{
    if ('post-thumbnail' === $size) {
        is_active_sidebar('sidebar-1') && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
        !is_active_sidebar('sidebar-1') && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
    }
    return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'figel_post_thumbnail_sizes_attr', 10, 3);

//quellio

function getSuffix($c) {
    if ($c > 1) {
        $c %= 100;
        if ($c == 1) {
            return 'ów';
        } else if ($c >= 2 && $c <= 4) {
            return 'y';
        } else if ($c >= 5 && $c <= 21) {
            return 'ów';
        } else {
            $c %= 10;
            if ($c >= 2 && $c <= 4) {
                return 'y';
            } else {
                return 'ów';
            }
        }
    } else {
        return '';
    }
}

function add_admin_menu_separator($position) {
    global $menu;
    $index = 0;
    foreach($menu as $offset => $section) {
        if (substr($section[2], 0, 9) == 'separator')
            $index++;
        if ($offset >= $position) {
            $menu[$position] = array('', 'read', "separator{$index}", '', 'wp-menu-separator');
            break;
        }
    }
    ksort( $menu );
}

function figel_acf_load_field( $field ) {
    $field['choices'] = array();
    $args = array(
        'post_type' => 'logotypes',
        'posts_per_page' => -1,
        'orderby' => 'post_title',
        'order' => 'ASC'
    );
    $lo_query = new WP_Query($args);
    if ($lo_query->have_posts()) :
        while ($lo_query->have_posts()) : $lo_query->the_post();
            global $post;
            $field['choices'][$post->post_name] = get_the_title();
        endwhile;
    endif;

    wp_reset_postdata();

    return $field;
}
add_filter('acf/load_field/name=product_brand','figel_acf_load_field');

function custom_menu_page_removing() {
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
    add_admin_menu_separator(25);
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

function get_post_by_title($post_name, $post_type = 'post', $output = OBJECT) {
    global $wpdb;
    $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type = %s", $post_name, $post_type ));
    if ( $post )
        return get_post($post, $output);

    return null;
}

/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function figel_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
        wp_die('No post to duplicate has been supplied!');
    }

    /*
     * get the original post id
     */
    $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
    /*
     * and all the original post data then
     */
    $post = get_post( $post_id );

    /*
     * if you don't want current user to be the new post author,
     * then change next couple of lines to this: $new_post_author = $post->post_author;
     */
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    /*
     * if post data exists, create the post duplicate
     */
    if (isset( $post ) && $post != null) {

        /*
         * new post data array
         */
        $args = array(
            'comment_status' => $post->comment_status,
            'ping_status'    => $post->ping_status,
            'post_author'    => $new_post_author,
            'post_content'   => $post->post_content,
            'post_excerpt'   => $post->post_excerpt,
            'post_name'      => $post->post_name,
            'post_parent'    => $post->post_parent,
            'post_password'  => $post->post_password,
            'post_status'    => 'draft',
            'post_title'     => $post->post_title,
            'post_type'      => $post->post_type,
            'to_ping'        => $post->to_ping,
            'menu_order'     => $post->menu_order
        );

        /*
         * insert the post by wp_insert_post() function
         */
        $new_post_id = wp_insert_post( $args );

        /*
         * get all current post terms ad set them to the new post draft
         */
        $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
        foreach ($taxonomies as $taxonomy) {
            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
            wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
        }

        /*
         * duplicate all post meta just in two SQL queries
         */
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
        if (count($post_meta_infos) != 0) {
            $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
            $sql_query.= implode(" UNION ALL ", $sql_query_sel);
            $wpdb->query($sql_query);
        }

        /*
         * finally, redirect to the edit post screen for the new draft
         */
        wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
        exit;
    } else {
        wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'figel_duplicate_post_as_draft' );

/*
 * Add the duplicate link to action list for post_row_actions
 */
function figel_duplicate_post_link( $actions, $post ) {
    if (current_user_can('edit_' . $post->post_type)) {
        $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplikuj produkt" rel="permalink">Duplikuj</a>';
    }
    return $actions;
}
add_filter( 'post_row_actions', 'figel_duplicate_post_link', 10, 2 );

function isValid()
{
    try {

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = ['secret' => RECAPTCHA_SECRET,
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']];

        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return json_decode($result)->success;
    } catch (Exception $e) {
        return null;
    }
}

function figel_phpmailer_init( PHPMailer $phpmailer ) {
    $phpmailer->Host = MAIL_SMTP_SERVER;
    $phpmailer->Port = MAIL_PORT; // could be different
    $phpmailer->Username = MAIL_USER; // if required
    $phpmailer->Password = MAIL_PSWD; // if required
    $phpmailer->SMTPAuth = MAIL_SMTP_AUTH; // if required
    $phpmailer->SMTPSecure = MAIL_SMTP_SECURE; // enable if required, 'tls' is another possible value
    $phpmailer->SMTPDebug = MAIL_SMTP_DEBUG;

    $phpmailer->isHTML(true);
    $phpmailer->isSMTP();

    $phpmailer->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
}
add_action( 'phpmailer_init', 'figel_phpmailer_init' );

remove_action('wp_head', 'wp_generator');

function quellio_secure_generator($generator, $type) {
    return '';
}
add_filter('the_generator', 'quellio_secure_generator', 10, 2);

function quellio_remove_src_version($src) {
    global $wp_version;
    $version_str = '?ver=' . $wp_version;
    $src = str_replace($version_str, '?ver=' . wp_hash($wp_version), $src);

    return $src;
}
add_filter('sript_loader_src', 'quellio_remove_src_version');
add_filter('style_loader_src', 'quellio_remove_src_version');

if ( is_admin() ) {
    include('admin/branch_management.php');
    include('includes/remove_metaboxes.php');
}
include('includes/register_cpt.php');
include('includes/functions_ania.php');
include('includes/figel_cart_form.php');
include('includes/figel_welding_form.php');

function nwtd_lpfs_custom_admin_query( $query ) {
    if( !is_admin() && !$query->is_main_query() ) {
        return;
    }
    if( is_post_type_archive( 'services' ) ) {
        $query->set('no_found_rows', 1 );
        $query->set('update_post_meta_cache', 0 );
        $query->set('update_post_term_cache', 0 );
    }
}
add_action( 'pre_get_posts', 'nwtd_lpfs_custom_admin_query' );


function prevent_post_deletion( $postid ) {
    $protected_post_id = 11;
    if ($postid == $protected_post_id) {
        header('Content-Type: text/html; charset=utf-8');
        exit('Tej strony nie można skasować');
    }
}
add_action('wp_trash_post', 'prevent_post_deletion');
add_action('before_delete_post', 'prevent_post_deletion', 10, 1);

//clear usused acf fields on save
function figel_clear_acf( $post_id ) {
    saveToLog($_POST, get_post_meta($post_id));
    if($_POST['acf'] != null && $_POST['action'] == 'editpost') {
        foreach (get_post_meta($post_id) as $key => $val) {
            if (substr($val[0], 0, 6) === 'field_' && !multi_array_key_exists($val[0], $_POST['acf'])) {
                update_post_meta($post_id, substr($key, 1, strlen($key) - 1), '');
            }
        }
    }
}
add_action( 'save_post', 'figel_clear_acf' );

function multi_array_key_exists($key, $arr, $depth = 1) {
    if ($key != null && $arr != null && is_array($arr)) {
        if (array_key_exists($key, $arr)) {
            return true;
        }
        foreach ($arr as $element) {
            if (is_array($element)) {
                if (multi_array_key_exists($key, $element, $depth + 1)) {
                    return true;
                }
            }
        }
    }
    return false;
}

add_filter('relevanssi_content_to_index', 'index_relational_acfs', 10, 2);
add_filter('relevanssi_excerpt_content', 'index_relational_acfs', 10, 2);

function index_relational_acfs($content, $post) {

    $acfs = array(
        array(
            'parent' => 'flex_content',
            'sub' => 'menadżer_produktu'
        ),
        'menadżer'
    );

    foreach ($acfs as $acf) {
        if (is_array($acf)) {
            if (have_rows($acf['parent'], $post->ID)) {
                while(have_rows($acf['parent'])) {
                    the_row();
                    $content .= get_sub_field($acf['sub'])->post_title;
                }
            }
        } else {
            $acf = get_field($acf, $post->ID);
            if ($acf) {
                $content .= $acf->post_title;
            }
        }
    }

    $product_cpts = array(
        'welding_device',
        'welding_material',
        'welding_handle',
        'plasma_device',
        'gas_welding_device',
        'helping_device',
        'welding_chemical',
        'abrasive_material',
        'soldering_device',
        'personal_protection',
        'dust_exhaust_device',
        'welding_protection',
        'welding_automation',
        'welding_robot',
        'control_device',
        'auto_warehouse',
    );

    if(in_array($post->post_type, $product_cpts)) {
        $manager = get_product_manager($post);
        $content .= $manager->post_title;
    }

    return $content;
}
function get_product_manager($post)
{
    $post_term = wp_get_post_terms($post->ID, $post->post_type . '_type')[0];
    if ($post_term) {
        if (!$meta = get_term_meta($post_term->term_id, 'branch-manager', true))
            if (!$meta = get_term_meta($post_term->parent, 'branch-manager', true))
                $meta = get_term_meta($post_term->term_id, 'parent-branch-manager', true);
    } else {
        $p = get_posts(
            array(
                'post_type' => 'page',
                'meta_key' => 'typ_postu_podrzednego',
                'meta_value' => $post->post_type
            )
        );
        $meta = get_post_meta($p[0]->ID, 'branch-manager', true);
    }
    return get_post($meta);
}

function saveToLog($pd, $pm) {
    $dir = __DIR__ . "/logs/";
    $log_file = $dir . "save_post_" . date('Y_m_d_H_i_s') . "_" . $pd['ID'] . ".txt";

    if (!is_dir($dir)) {
        mkdir($dir);
    }
    
	$content = "";

    $content .=
        "post ID:\t" . $pd['ID'] . PHP_EOL .
        "post title:\t" . $pd['post_title'] . PHP_EOL .
        "post type:\t" . $pd['post_type'] . PHP_EOL .
        "action:\t\t" . $pd['action'] . PHP_EOL . PHP_EOL;
    $content .= '$_POST["acf"]' . PHP_EOL . array_to_text($pd['acf']);
    $content .= 'post_meta' . PHP_EOL . array_to_text($pm);

    file_put_contents($log_file, $content);
}

function array_to_text($array, $depth = 1) {
    $txt = "";
    $indent = "";
    for($i = 0; $i < $depth; $i++) {
        $indent .= "\t";
    }
	if(is_array($array)) {
		foreach($array as $key => $val) {
			if(is_array($val))
				$txt .= $indent . $key . "\t" . $val . PHP_EOL . array_to_text($val, $depth + 1);
			else {
				$txt .= $indent . $key . " = " . $val . PHP_EOL;
			}
		}
	} else {
		$txt .= $indent . $key . " = " . $array . PHP_EOL;
	}
    return $txt;
}