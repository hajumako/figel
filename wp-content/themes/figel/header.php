<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" itemscope itemtype="http://schema.org/Article">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#025ba1">
    <meta name="theme-color" content="#ffffff">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Figel">
    <meta name="application-name" content="Figel">
    <meta name="theme-color" content="#ffffff">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
    
    <script>
        //FACEBOOK SCRIPT

        window.fbAsyncInit = function () {
            FB.init({
                appId: '304910406544412',
                xfbml: true,
                version: 'v2.7'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        // LOADING SDK

        //        console.log('sdk');
        //        (function () {
        //            var e = document.createElement('script');
        //            e.async = true;
        //            e.src = document.location.protocol + '//connect.facebook.net/pl_PL/all.js';
        //            document.getElementById('fb-root').appendChild(e);
        //        }());

        //END FACEBOOK SCRIPT
    </script>
</head>

<body <?php body_class(); ?>>
<div class="body-bg"></div>
<div id="page">
    <!--<a class="skip-link screen-reader-text" href="#content">--><?php //_e('Skip to content', 'twentysixteen'); ?><!--</a>-->

    <header class="site-header" role="banner">

        <div class="site-content">
            <div class="header-logo">
                <a class="site-title" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img src="<?php echo get_template_directory_uri()?>/images/logo-figel.svg" alt="Logo Figel"/></a>
            </div>
            <div class="navigation">
                <?php if (has_nav_menu('top_main') || has_nav_menu('social')) : ?>

                    <?php if (has_nav_menu('top_main')) : ?>
                        <nav id="main-navigation" role="navigation" aria-label="Menu główne">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'top_main',
                                'menu_class' => 'top-menu',
                            ));
                            ?>
                        </nav>
                    <?php endif; ?>

                    <?php if (has_nav_menu('lang')) : ?>
                        <nav id="lang-navigation" role="navigation" aria-label="Wybór języków">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'lang',
                                'menu_class' => 'lang-menu',
                            ));
                            ?>
                        </nav>
                    <?php endif; ?>

                    <?php if (has_nav_menu('mmenu')) : ?>
                        <nav id="menu-mobile" role="navigation" aria-label="Menu mobilne" class="mmenu-display">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'mmenu',
                                'menu_class' => 'mobile-menu',
                            ));
                            ?>
                            <span>
                                 <a class="mm-close-menu"></a>
                            </span>
                        </nav>
                    <?php endif; ?>

                <?php endif; ?>

                <?php if (is_active_sidebar('top-widget')) : ?>
                    <div id="search-navigation" role="search">
                        <?php dynamic_sidebar('top-widget'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <a class="mm-button" href="#menu-mobile">
                <span id="l1"></span>
                <span id="l2"></span>
                <span id="l3"></span>
            </a>
        </div>
        <?php $c = isset($_COOKIE['figel_cart']) ? count(explode(',', trim($_COOKIE['figel_cart'], ','))) : 0; ?>
        <div class="cart" style="<?php echo $c == 0 ? 'display:none' : ''; ?>">
            <a href="<?php the_permalink(362); ?>">
                <h4>Zapytanie ofertowe:</h4>
                <p class="products"><?php echo $c . ' produkt' . getSuffix($c); ?></p>
            </a>
        </div>
    </header>

    <?php if (get_the_ID() != get_page_by_title("Formularz łączenia i cięcia")->ID): ?>

            <a class="form-laczenia-ciecia" href="<?php the_permalink(get_page_by_title("Formularz łączenia i cięcia")->ID); ?>">
                <div class="icon"></div>
                <p>Chciałbyś coś połączyć lub przeciąć?</p>
            </a>

    <?php endif; ?>


<!--    --><?php //$main_page_id = get_option('page_on_front');?>

    <?php if (!is_front_page()): ?>
        <div id="banner" class="banner <?php if (is_404()) { echo 'error-banner';}?>" ></div>
    
        <div id="content" class="site-content">
    <?php endif; ?>