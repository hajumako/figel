<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

</div>

<footer id="colophon" class="site-footer" role="contentinfo">
    <?php if(!is_404()):?>
    <div id="top">
        <div class="site-content">
            <h4>Autoryzowany dystrybutor:</h4>
            <ul class="bxslider" style="display: none;">
                <?php
                $args = array('post_type' => 'logotypes', 'posts_per_page' => -1);
                $loop = new WP_Query($args);
                ?>
                <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                    <?php if (has_post_thumbnail()) : ?>
                        <?php if (get_field('show_in_footer')) : ?>
                            <li>
                                <?php if(get_field('link', get_the_ID())) : ?>
                                    <a href="<?php the_field('link', get_the_ID()); ?>" target="_blank">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                <?php else: ?>
                                    <?php the_post_thumbnail(); ?>
                                <?php endif; ?>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
    <?php endif;?>

    <div id="middle">
        <div class="site-content">
            <?php if (has_nav_menu('social')) : ?>
                <nav class="social-navigation" role="navigation" aria-label="Media społecznościowe">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'social',
                        'menu_class' => 'social-menu',
                    ));
                    ?>
                </nav>
            <?php endif; ?>
            <a href="<?php the_permalink(get_page_by_title('Praca')->ID); ?>" class="job"><p><span>Praca w spawalnictwie!</span> <br/> Sprawdź kogo aktualnie poszukujemy</p> </a>
        </div>
    </div>

    <div id="bottom">
        <div class="site-content">
            <p>Copyright &copy; <?php the_time('Y'); ?> FIGEL Sp. z o.o.</p>
            <?php if (has_nav_menu('bottom')) : ?>
                <nav class="bottom-navigation" role="navigation" aria-label="Menu w stopce">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'bottom',
                        'menu_class' => 'bottom-menu',
                    ));
                    ?>
                </nav>
            <?php endif; ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/quellio.png" alt="Quellio Logo"/>
        </div>
    </div>

</footer>

</div> <!--page -->
<script type="text/javascript">
    jQuery(document).ready(function(){

        jQuery('.facebook').click(function(e){
            e.preventDefault();

            $post = jQuery(this).parents('article');
            console.log($post);
            $name = $post.find('.title').text();
            console.log($name);
            $caption = '';
            $descr = $post.find('.text').text();
            console.log($descr);
            $link = window.location.href;
            console.log($link);
            $title = jQuery(this).parents('html').find('head').find('title').html();
            console.log($title);

            if ($post.find('.image').length != 0){
                $picture = $post.find('.image img').first().attr('src');
                console.log($picture);

            } else {
                $picture = '';

            }


           FB.ui({
                method: 'feed',
                name: $name,
                link: $link,
                picture: $picture,
                caption: $title,
                description: $descr
            });
        });
    });

</script>
<?php wp_footer(); ?>
</body>

</html>
