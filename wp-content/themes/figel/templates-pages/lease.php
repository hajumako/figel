<?php
/**
 * Template Name: Dzierżawa
 *
 *
 */

$redirect = get_field('redirect', $post->ID);
if ($redirect)
    header('Location: ' . $redirect);

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page">

            <div class="normal-page-content lease">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                        <?php get_template_part('template-parts/content', 'flex'); ?>
                        <?php if (get_field('produkty')): ?>
                            <table class="table-lease">
                                <tbody>
                                <tr>
                                    <th>Metoda</th>
                                    <th></th>
                                    <th>Nazwa</th>
                                    <th>Wyposażenie</th>
                                    <th>Stawka</th>
                                </tr>
                                <?php foreach (get_field('produkty') as $p): ?>
                                    <tr>
                                        <td>
                                            <?php echo $p['metoda']; ?>
                                        </td>
                                        <td>
                                            <?php if($p['produkt']) : ?>
                                                <a href="<?php the_permalink($p['produkt']->ID); ?>">
                                                    <?php $image = wp_get_attachment_image(get_field('zdjęcia', $p['produkt']->ID)[0]['id'], 'thumbnail'); ?>
                                                    <?php if($image): ?>
                                                        <div class="image-wrapper">
                                                            <?php echo $image; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </a>
                                            <?php else : ?>
                                                <?php $image = wp_get_attachment_image($p['zdjęcie']['id'], 'thumbnail'); ?>
                                                <?php if($image): ?>
                                                    <div class="image-wrapper">
                                                        <?php echo $image; ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="name">
                                            <?php if($p['produkt']) : ?>
                                                <a href="<?php the_permalink($p['produkt']->ID); ?>">
                                                    <h3>
                                                        <?php echo $p['produkt']->post_title; ?>
                                                        <span><?php echo $p['opis']; ?></span>
                                                    </h3>
                                                </a>
                                            <?php else : ?>
                                                <h3>
                                                    <?php echo $p['produkt_inny']; ?>
                                                    <span><?php echo $p['opis']; ?></span>
                                                </h3>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php echo $p['wyposazenie']; ?>
                                        </td>
                                        <td class="rate">
                                            <?php echo $p['stawka']; ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php get_template_part('template-parts/content', 'none'); ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>