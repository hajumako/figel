<?php
/**
 * Template Name: Kontakt - ogólny
 *
 * Główne dane teleadresowe
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('template-parts/content', 'map'); ?>
        <div class="content-wrapper normal-page">
            <?php get_template_part('template-parts/side', 'siblings'); ?>
            <div class="normal-page-content">
                <h2><?php echo $post->post_title; ?></h2>
                <?php 
                $offices = get_posts(array(
                    'post_type' => 'office',
                    'posts_per_page' => -1
                ));
                ?>
                <?php if ($offices) : ?>
                    <div id="offices">
                        <?php $wrapped_offices = 0; ?>
                        <?php foreach ($offices as $i => $office): ?>
                            <?php if($wrapped_offices++ == 0): ?>
                                <ul class="row-offices">
                            <?php endif ;?>
                            <li data-target="<?php echo $i; ?>" class="person">
                                <h3><?php echo $office->post_title; ?></h3>
                                <?php  $wrapped_items = 0; ?>
                                <?php foreach (get_fields($office->ID) as $name => $field): ?>
                                    <?php if ($name == 'adres' && $field != ''): ?>
                                       <div class="description"><?php echo $field; ?></div>
                                    <?php elseif ($name == 'email' && $field != ''): ?>
                                        <?php if($wrapped_items++ == 0): ?>
                                            <div class="linki">
                                        <?php endif;?>
                                        <a href="mailto:<?php echo $field; ?>"><span>E:</span> <?php echo $field; ?></a>
                                    <?php elseif ($name == 'nr_telefonow'): ?>
                                        <?php foreach ($field as $f): ?>
                                            <?php if ($f != ''): ?>
                                                <?php if($wrapped_items++ == 0): ?>
                                                    <div class="linki">
                                                 <?php endif; ?>
                                                <a href="tel:<?php echo $f['telefon']; ?>"><span><?php echo $f['rodzaj_telefonu']; ?></span> <?php echo $f['telefon']; ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                 <?php if($wrapped_items != 0): ?>
                                     </div>
                                  <?php endif;?>
                            </li>
                            <?php if(($wrapped_offices %= 2) == 0): ?>
                                </ul>
                            <?php endif;?>
                        <?php endforeach; ?>
                        <?php if($wrapped_offices % 2 != 0): ?>
                            </div>
                        <?php endif;?>
                    </div>
                <?php endif; ?>
                <?php get_template_part('template-parts/content', 'flex'); ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
