<?php
/**
 * Template Name: Zapytanie ofertowe
 *
 * Lista produktów
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page ">
            <div class="normal-page-content form-page full-width">
                <h2><?php the_title(); ?></h2>
                <nav class="nav-form">
                    <ul>
                        <li>
                            <a href="#step1" data-step="step1" class="active">Lista produktów</a>
                        </li>
                        <li>
                            <a href="#step2" data-step="step2">Dane kontaktowe</a>
                        </li>
                    </ul>
                </nav>
                <form id="form-figel" method="post" action="<?php echo $link = admin_url('admin-ajax.php?action=send_cart_form'); ?>">
                    <input type="hidden" name="action" value="send_cart_form"/>
                    <div id="step1" class="step-wrapper product-list active">
                        <h4 class="step-title">Lista produktów</h4>
                        <ul>
                            <?php if (trim($_COOKIE['figel_cart'], ',') != ''): ?>
                                <?php foreach (explode(',', $_COOKIE['figel_cart']) as $i => $product): ?>
                                    <?php $post = get_post(str_replace('post-', '', $product)); ?>
                                    <li>
                                        <input class="product" type="hidden" name="products[]" value="<?php echo $post->ID; ?>"/>
                                        <div class="counter"><?php echo $i + 1; ?></div>
                                        <div class="dummy-div"></div>
                                        <div class="img-wrapper">
                                            <?php echo wp_get_attachment_image(get_field('zdjęcia', $post->ID)[0]['id']); ?>
                                        </div>
                                        <p class="name"><?php the_title(); ?></p>
                                        <button type="button" class="delete_from_cart" data-id="<?php echo $product; ?>">Usuń</button>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                        <p id="errorStep1" class="globalError">Brak produktów na liście</p>
                        <button type="button" class="next-step more" data-step="step2">Dalej</button>
                    </div>
                    <div id="step2" class="step-wrapper">
                        <h4 class="step-title">Dodatkowe informacje</h4>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Imię i nazwisko<span class="star">*</span></span>
                            <input class="userInput required" type="text" name="f_name" data-name="Imię i nazwisko" value=""/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Firma</span>
                            <input class="userInput" type="text" name="f_company" data-name="Firma"/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Adres e-mail<span class="star">*</span></span>
                            <input class="userInput required" id="email" type="text" name="f_email" data-name="Email"/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Telefon<span class="star">*</span></span>
                            <input class="userInput required" id="phone" type="text" name="f_phone" data-name="Telefon"/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Uwagi</span>
                            <textarea class="userInput" name="f_additional_info" cols="80" rows="7"></textarea>
                        </div>
                        <div class="g-recaptcha inputWrapper" data-callback="recaptchaCallback" data-sitekey="<?php echo RECAPTCHA_SITEKEY; ?>"></div>
                        <noscript>
                            <div style="width: 302px; height: 425px; position: relative;margin: 0 auto 30px">
                                <div style="width: 302px; height: 425px; position: absolute;">
                                    <iframe src="https://www.google.com/recaptcha/api/fallback?k=<?php echo RECAPTCHA_SITEKEY; ?>" scrolling="no"
                                            style="width: 302px; height:425px; border-style: none;">
                                    </iframe>
                                </div>
                            </div>
                            <div>
                                <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" cols="90" rows="6" style="font-size:11px"></textarea>
                            </div>
                        </noscript>
                        <p id="errorStep2" class="globalError">Wypełnij wszystkie wymagane pola</p>
                        <div class="info-wrap-form">
                            <p class="info"><span class="star">* </span> Pola obowiązkowe</p>
                            <input type="submit" value="Wyślij" class="send"/>
                        </div>

                    </div>
                </form>
                
                <script>
                    site_url = '<?php echo bloginfo('url'); ?>';
                </script>
                <div id="busy"  class="result-msg" style="display: none"></div>
                <div id="success" class="result-msg" <?php echo (isset($_GET['result_type']) && $_GET['result_type'] == 'success') ? '' : 'style="display: none"'; ?>>
                    <p>Dziękujemy, Twoja wiadomość została wysłana poprawnie!</p>
                </div>
                <div id="error" class="result-msg" <?php echo (isset($_GET['result_type']) && $_GET['result_type'] == 'error') ? '' : 'style="display: none"'; ?>>
                    <p>Niestety, nie udało się wysłać formularza!</p>
                    <?php if (isset($_GET['code'])): ?>
                        <?php if ($_GET['code'] == 101): ?>
                            <?php //błąd przy wysyłce maila ?>
                        <?php elseif ($_GET['code'] == 102): ?>
                            <p>Formularz zawiera błędy. Uzupełnij wszystkie obowiązkowe pola.</p>
                        <?php elseif ($_GET['code'] == 103): ?>
                            <p>Niepoprawna recaptcha.</p>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
