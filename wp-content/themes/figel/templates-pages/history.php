<?php
/**
 * Template Name: Historia
 *
 * Lista artykułów
 *
 */

$redirect = get_field('redirect', $post->ID);
if ($redirect)
    header('Location: ' . $redirect);

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page">
            <?php get_template_part('template-parts/side', 'siblings'); ?>

            <div class="normal-page-content history">
                <h2><?php the_title(); ?></h2>
                <?php $x = get_field('history_flex_content'); ?>
                <?php if (get_field('history_flex_content')): ?>
                    <?php $lightbox = 0; ?>
                    <?php while (has_sub_field('history_flex_content')): ?>
                        <?php if (get_row_layout() == 'data'): ?>
                            <div class="date">
                                <h3><?php the_sub_field('rok'); ?></h3>
                            </div>
                        <?php elseif (get_row_layout() == 'treść'): ?>
                            <div class="date-content">

                                <?php while (has_sub_field('treści')): ?>
                                    <?php if (get_row_layout() == 'zwykły_tekst'): ?>
                                        <div class="text">
                                            <?php the_sub_field('tekst'); ?>
                                        </div>
                                    <?php elseif (get_row_layout() == 'zdjęcie'): ?>
                                        <div class="image">
                                            <?php $image = get_sub_field('obrazek'); ?>
                                            <a href="<?php echo $image['sizes']['large']; ?>" data-lightbox= "image<?php echo $lightbox;?>"><img width="300" src="<?php echo $image['url']; ?>"
                                                                                                                         title="<?php echo $image['title']; ?>"
                                                                                                                         alt="<?php echo $image['title']; ?>"/></a>
<!--                                            <h6>--><?php //the_sub_field('podpis'); ?><!--</h6>-->
                                        </div>
                                    <?php elseif (get_row_layout() == 'wideo'): ?>
                                        <div class="video">
                                            <?php the_sub_field('link_do_filmu'); ?>
                                            <h6><?php the_sub_field('podpis'); ?></h6>
                                        </div>
                                    <?php endif; ?>

                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                        <?php $lightbox++; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
