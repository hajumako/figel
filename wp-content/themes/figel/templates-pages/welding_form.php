<?php
/**
 * Template Name: Formularz - co chcesz zrobić?
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page ">
            <div class="normal-page-content form-page full-width">
                <h2><?php the_title(); ?></h2>
                <nav class="nav-form">
                    <ul>
                        <li>
                            <a href="#step1" data-step="step1" class="active">Krok 1</a>
                        </li>
                        <li>
                            <a href="#step2" data-step="step2">Krok 2</a>
                        </li>
                        <li>
                            <a href="#step3" data-step="step3">Krok 3</a>
                        </li>
                    </ul>
                </nav>
                <form id="form-figel" method="post" action="<?php echo $link = admin_url('admin-ajax.php?action=send_welding_form'); ?>">
                    <input type="hidden" name="action" value="send_welding_form"/>
                    <div id="step1" class="step-wrapper active">
                        <p>
                            Jeśli wiesz, co chcesz zrobić, ale nie wiesz, jakiego sprzętu potrzebujesz wypełnij poniższy formularz. Nasi specjaliści pomogą Ci dobrać odpowiedni
                            sprzęt.
                        </p>
                        <div class="inputWrapper inline">
                            <h4>Proces<span class="star">*</span></h4>
                            <label>
                                <input class="userInput required" type="radio" name="process_type" value="con">
                                <span>Łączenie materiałów</span>
                            </label>
                            <label>
                                <input class="userInput required" type="radio" name="process_type" value="cut">
                                <span>Cięcie</span>
                            </label>
                        </div>
                        <br>
                        <div class="info-wrap">
                            <p class="info"><span class="star">* </span> Pola obowiązkowe</p>
                            <button type="button" class="next-step more" data-step="step2">Dalej</button>
                        </div>
                    </div>
                    <div id="step2" class="step-wrapper">
                        <p>
                            W celu przygotowania oferty dostosowanej do Twoich potrzeb wypełnij poniższe pola.
                        </p>
                        <div id="con" class="conditional-show">
                            <h4 class="step-title">Łączenie materiałów</h4>
                            <div class="inputWrapper inline">
                                <h4>Szukam<span class="star">*</span></h4>
                                <label>
                                    <input class="userInput required" type="checkbox" name="con_need[]" value="device">
                                    <span>Urządzeń</span>
                                </label>
                                <label>
                                    <input class="userInput required" type="checkbox" name="con_need[]" value="material">
                                    <span>Materiałów</span>
                                </label>
                            </div>
                            <br>
                            <div class="boxedWrapper">
                                <div class="boxed">
                                    <div class="inputWrapper inline">
                                        <h4>Gatunek materiału 1<span class="star">*</span></h4>
                                        <?php foreach ($con_material1_type as $v => $l) : ?>
                                            <label class="list">
                                                <input class="userInput required" type="checkbox" name="con_material1_type[]" value="<?php echo $v; ?>">
                                                <span><?php echo $l; ?></span>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="inputWrapper bordered">
                                        <div>
                                            <h4>Grubość materiału 1<span class="star">*</span></h4>
                                            <input class="userInput required" type="number" name="con_material1_thickness"><span>mm</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="boxed">
                                    <div class="inputWrapper inline">
                                        <h4>Gatunek materiału 2</h4>
                                        <?php foreach ($con_material2_type as $v => $l) : ?>
                                            <label class="list">
                                                <input class="userInput" type="checkbox" name="con_material2_type[]" value="<?php echo $v; ?>">
                                                <span><?php echo $l; ?></span>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="inputWrapper bordered">
                                        <h4>Grubość materiału 2</h4>
                                        <input class="userInput" type="number" name="con_material2_thickness"><span>mm</span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="inputWrapper inline">
                                    <h4>Preferowana metoda łączenia<span class="star">*</span></h4>
                                    <label>
                                        <input class="userInput required" type="radio" name="method_type" value="welding">
                                        <span>Spawanie</span>
                                    </label>
                                    <label>
                                        <input class="userInput required" type="radio" name="method_type" value="soldering">
                                        <span>Zgrzewanie</span>
                                    </label>
                                    <label>
                                        <input class="userInput required" type="radio" name="method_type" value="no_idea">
                                        <span>Nie wiem</span>
                                    </label>
                                </div>
                                <div class="border"></div>
                                <div id="welding_methods" class="conditional-show inputWrapper bordered">
                                    <div class="field-info">Wybierz maksymalnie 2 opcje</div>
                                    <?php foreach ($welding_methods as $v => $l) : ?>
                                        <label class="list">
                                            <input class="userInput" type="checkbox" name="welding_method[]" value="<?php echo $v; ?>">
                                            <span><?php echo $l; ?></span>
                                        </label>
                                    <?php endforeach; ?>
                                </div>
                                <div id="soldering_methods" class="conditional-show inputWrapper bordered">
                                    <?php foreach ($soldering_methods as $v => $l) : ?>
                                        <label class="list">
                                            <input class="userInput" type="radio" name="soldering_method[]" value="<?php echo $v; ?>">
                                            <span><?php echo $l; ?></span>
                                        </label>
                                    <?php endforeach; ?>
                                </div>
                                <div class="border"></div>
                            </div>
                            <div class="inputWrapper inline">
                                <h4>Uwagi</h4>
                                <textarea name="additional_info" cols="80" rows="7"></textarea>
                            </div>
                        </div>
                        <div id="cut" class="conditional-show">
                            <h4 class="step-title">Cięcie</h4>
                            <div class="inputWrapper inline">
                                <h4>Szukam<span class="star">*</span></h4>
                                <label>
                                    <input class="userInput required" type="checkbox" name="cut_need[]" value="device">
                                    <span>urządzeń</span>
                                </label>
                                <label>
                                    <input class="userInput required" type="checkbox" name="cut_need[]" value="equipment">
                                    <span>osprzętu</span>
                                </label>
                            </div>
                            <div class="boxedWrapper one-column" >
                                <div class="boxed">
                                    <div class="inputWrapper inline">
                                        <h4>Gatunek materiału ciętego<span class="star">*</span></h4>
                                        <?php foreach ($cut_material_type as $v => $l) : ?>
                                            <label class="list">
                                                <input class="userInput required" type="checkbox" name="cut_material_type[]" value="<?php echo $v; ?>">
                                                <span><?php echo $l; ?></span>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="inputWrapper bordered">
                                        <h4>Grubość materiału ciętego<span class="star">*</span></h4>
                                        <input class="userInput required" type="number" name="cut_material_thickness"><span>mm</span>
                                    </div>
                                </div>
                            </div>
                            <div class="inputWrapper inline">
                                <h4>Rodzaj cięcia</h4>
                                <label>
                                    <input class="userInput" type="radio" name="cut_method" value="ręczne">
                                    <span>ręczne</span>
                                </label>
                                <label>
                                    <input class="userInput" type="radio" name="cut_method" value="zmechanizowane">
                                    <span>zmechanizowane</span>
                                </label>
                            </div>
                            <div class="inputWrapper inline">
                                <h4>Uwagi</h4>
                                <textarea name="cut_additional_info" cols="80" rows="7"></textarea>
                            </div>
                        </div>
                        <p id="errorStep2" class="globalError">Wypełnij wszystkie wymagane pola</p>
                        <div class="info-wrap">
                            <p class="info"><span class="star">* </span> Pola obowiązkowe</p>
                            <button type="button" class="next-step more" data-step="step3">Dalej</button>
                        </div>
                    </div>
                    <div id="step3" class="step-wrapper">
                        <h4 class="step-title">Dodatkowe informacje</h4>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Imię i nazwisko<span class="star">*</span></span>
                            <input class="userInput required" type="text" name="f_name" data-name="Imię i nazwisko" value=""/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Firma</span>
                            <input class="userInput" type="text" name="f_company" data-name="Firma"/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Adres e-mail<span class="star">*</span></span>
                            <input class="userInput required" id="email" type="text" name="f_email" data-name="Email"/><br/>
                        </div>
                        <div class="placeholder inputWrapper">
                            <span class="holder">Telefon<span class="star">*</span></span>
                            <input class="userInput required" id="phone" type="text" name="f_phone" data-name="Telefon"/><br/>
                        </div>
                        <div class="g-recaptcha inputWrapper" data-callback="recaptchaCallback" data-sitekey="<?php echo RECAPTCHA_SITEKEY; ?>"></div>
                        <noscript>
                            <div style="width: 302px; height: 425px; position: relative;margin: 0 auto 30px">
                                <div style="width: 302px; height: 425px; position: absolute;">
                                    <iframe src="https://www.google.com/recaptcha/api/fallback?k=<?php echo RECAPTCHA_SITEKEY; ?>" scrolling="no"
                                            style="width: 302px; height:425px; border-style: none;">
                                    </iframe>
                                </div>
                            </div>
                            <div>
                                <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" cols="90" rows="6" style="font-size:11px"></textarea>
                            </div>
                        </noscript>
                        <p id="errorStep3" class="globalError">Wypełnij wszystkie wymagane pola</p>
                        <div class="info-wrap-form">
                            <p class="info"><span class="star">* </span> Pola obowiązkowe</p>
                            <input type="submit" value="Wyślij" class="send"/>
                        </div>
                    </div>
                </form>

                <script>
                    site_url = '<?php bloginfo('url'); ?>';
                </script>
                <div id="busy" class="result-msg" style="display: none"></div>
                <div id="success" class="result-msg" <?php echo (isset($_GET['result_type']) && $_GET['result_type'] == 'success') ? '' : 'style="display: none"'; ?>>
                    <p>Dziękujemy, Twoja wiadomość została wysłana poprawnie!</p>
                </div>
                <div id="error" class="result-msg" <?php echo (isset($_GET['result_type']) && $_GET['result_type'] == 'error') ? '' : 'style="display: none"'; ?>>
                    <p>Niestety, nie udało się wysłać formularza!</p>
                    <?php if (isset($_GET['code'])): ?>
                        <?php if ($_GET['code'] == 101): ?>
                            <?php //błąd przy wysyłce maila ?>
                        <?php elseif ($_GET['code'] == 102): ?>
                            <p>Formularz zawiera błędy. Uzupełnij wszystkie obowiązkowe pola.</p>
                        <?php elseif ($_GET['code'] == 103): ?>
                            <p>Niepoprawna recaptcha.</p>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
