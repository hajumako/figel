<?php
/**
 * Template Name: Przekierowanie
 *
 * Przekierowanie do pierwszego potomka
 *
 */


$children = get_pages(
    array(
        'child_of' => get_the_ID(),
        'sort_column' => 'menu_order',
        'sort_order' => 'ASC'
));
if(count($children))
    header('Location: ' . get_permalink($children[0]->ID));