<?php
/**
 * Template Name: Kontakt - podstrony
 *
 * Lista pracowników w działach/oddziałach
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('template-parts/content', 'map'); ?>
        <div class="content-wrapper normal-page">
            <?php get_template_part('template-parts/side', 'siblings'); ?>
            <div class="normal-page-content">
                <h2><?php echo $post->post_title; ?></h2>
                <?php get_template_part('template-parts/content', 'flex'); ?>
                <?php $employees = get_field('lista_pracownikow'); ?>
                <?php if ($employees) : ?>
                    <div id="employees">
                        <?php
                        $wrapped_offices = 0;
                        foreach ($employees as $employee): ?>
                            <?php if($wrapped_offices++ == 0): ?>
                                <ul class="row-employees">
                            <?php endif;?>
                            <li class="person">
                                <h3><?php echo $employee['pracownik']->post_title; ?></h3>
                                <?php
                                $wrapped_items = 0;
                                foreach (get_fields($employee['pracownik']->ID) as $name => $field): ?>
                                    <?php if ($name == 'stanowisko' && $field != ''): ?>
                                        <p class="stanowisko"><?php echo $field; ?></p>
                                    <?php elseif ($name == 'dodatkowy_podpis' && $field != ''): ?>
                                        <div class="description"><?php echo $field; ?></div>
                                    <?php elseif ($name == 'email' && $field != ''): ?>
                                        <?php if($wrapped_items++ == 0): ?>
                                            <div class="linki">
                                        <?php endif; ?>
                                        <a href="mailto:<?php echo $field; ?>">
                                            <span>E: </span> <?php echo $field; ?>
                                        </a>
                                    <?php elseif ($name == 'nr_telefonow' && $field != ''): ?>
                                        <?php foreach ($field as $f): ?>
                                            <?php if ($f != ''): ?>
                                                <?php if($wrapped_items++ == 0): ?>
                                                    <div class="linki">
                                                 <?php endif ;?>
                                                <a href="tel:<?php echo $f['telefon']; ?>">
                                                    <span><?php echo $f['rodzaj_telefonu']; ?></span> <?php echo $f['telefon']; ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if($wrapped_items != 0): ?>
                                    </div>
                                <?php endif;?>
                            </li>
                            <?php if(($wrapped_offices %= 2) == 0): ?>
                                </ul>
                            <?php endif;?>
                        <?php endforeach; ?>
                        <?php if($wrapped_offices % 2 != 0): ?>
                            </ul>
                         <?php endif;?>
                        <?php $locations .= ']'; ?>
                    </div>
                    <?php echo '<script>var locations = ' . $locations . '</script>'; ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
