<?php
/**
 * Template Name: Strona główna
 */

get_header(); ?>
<div id="banner" class="main-page-banner" style="background-image: url(<?php the_post_thumbnail_url(array(1920, 530));?>)">
    <div class="site-content">
        <div class="slogan">
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            ?>
        </div>
    </div>
</div>
<div id="primary" class="content-area site-content">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper">

            <h2 class="offer-header"><?php pll_e('Oferta') ?></h2>
            <div></div>
            <div class="main-page-offer">
                <ul>
                    <?php if (get_field('podstrony')): ?>
                        <?php foreach (get_field('podstrony') as $p): ?>
                            <li>
                                <a href="<?php the_permalink($p['podstrona']); ?>">
                                    <?php echo $p['etykieta'] ? $p['etykieta'] : $p['podstrona']->post_title; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div></div>
            <div class="main-page-news">
                <ul class="bxslider-recent-news" style="display: none;">
                    <?php
                    $recent_posts = get_posts(
                        array(
                            'post_type' => 'news',
                            'posts_per_page' => 3,
                            'post__not_in' => array(get_the_ID()),
                            'orderby' => 'date',
                            'order' => 'DESC'
                        ));

                    foreach ($recent_posts as $recent) {
                        $title_in = $recent->post_title;
                        $title_out = strlen($title_in) > 50 ? substr($title_in, 0, 50)."..." : $title_in;
                        $excerpt_in = $recent->post_excerpt;
                        $excerpt_out = strlen($excerpt_in) > 190 ? substr($excerpt_in, 0, 190)."..." : $excerpt_in;

                        echo '<li><h4><a href="' . get_permalink($recent->ID) . '">' . $title_out . '</a><div class="dummy-div"></div> </h4> ';
                        echo '<p class="date">' . mysql2date('j F Y', $recent->post_date) . '</p> ';
                        echo '<p>' . $excerpt_out . '</p> ';
                        echo '<a class="more" href="' . get_permalink($recent->ID) . '">Więcej</a></li>';
                    }
                    wp_reset_query();
                    ?>
                </ul>
            </div>
            <div></div>

        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
