<?php
/**
 * Template Name: Oferty pracy
 *
 */

$redirect = get_field('redirect', $post->ID);
if ($redirect)
    header('Location: ' . $redirect);

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page">
            <?php //get_template_part('template-parts/side', 'siblings'); ?>
            <?php include( locate_template('template-parts/side-siblings.php')); ?>
            <div class="news-list articles">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
<!--                        <h2>--><?php //the_title(); ?><!--</h2>-->
                        <?php get_template_part('template-parts/content', 'flex'); ?>
                    <?php endwhile; ?>

                    <?php get_template_part('template-parts/content', 'jobs'); ?>
                <?php else : ?>
                    <?php get_template_part('template-parts/content', 'none'); ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>

