<?php
/**
 * Template Name: Wiedza - przegląd technologi
 *
 * Przegląd technologii spawalniczych
 *
 */

$redirect = get_field('redirect', $post->ID);
if ($redirect)
    header('Location: ' . $redirect);

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area <?php if ($wp_query->max_num_pages > 1) echo 'paged';?>">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page">
            <?php get_template_part('template-parts/side', 'siblings'); ?>

            <div class="normal-page-content">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <?php get_template_part('template-parts/content', 'flex'); ?>

                        <?php get_template_part('template-parts/content', 'technologies'); ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php get_template_part('template-parts/content', 'none'); ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>

