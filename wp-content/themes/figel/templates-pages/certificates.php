<?php
/**
 * Template Name: Certyfikaty i Referencje
 *
 *
 */
get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page">
            <?php get_template_part('template-parts/side', 'siblings'); ?>
            <div class="normal-page-content">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <?php get_template_part('template-parts/content', 'documents'); ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php get_template_part('template-parts/content', 'none'); ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>