<?php
/**
 * Template Name: Mapa strony
 *
 *
 */
get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="content-wrapper normal-page ">
                <?php get_template_part('template-parts/side', 'siblings'); ?>
                <div class="normal-page-content sitemap">
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <h2><?php the_title(); ?></h2>

                            <?php
                            $pages = get_posts(
                                array(
                                    'posts_per_page' => -1,
                                    'post_type' => 'page',
                                    'post_parent' => 0,
                                    'orderby' => 'menu_order',
                                    'order' => 'ASC',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'language',
                                            'field' => 'slug',
                                            'terms' => pll_current_language('slug'),
                                        ),
                                    ),
                                ));
                            ?>
                            <?php if ($pages) : ?>
                                <ul>
                                    <?php foreach ($pages as $page) : ?>
                                        <li>
                                            <a href="<?php the_permalink($page->ID); ?>">
                                                <?php echo $page->post_title; ?>
                                            </a>

                                            <?php
                                            if ($page->ID == 11)
                                                $args = array(
                                                    'posts_per_page' => -1,
                                                    'post_type' => 'page',
                                                    'post_parent' => $page->ID,
                                                    'orderby' => 'meta_value',
                                                    'order' => 'ASC',
                                                    'meta_key' => 'branch-letter'
                                                );
                                            else
                                                $args = array(
                                                    'posts_per_page' => -1,
                                                    'post_type' => 'page',
                                                    'post_parent' => $page->ID,
                                                    'orderby' => 'menu_order',
                                                    'order' => 'ASC'
                                                );
                                            $subpages = get_posts($args);
                                            ?>
                                            <?php if ($subpages) : ?>
                                                <ul class="subpage">
                                                    <?php foreach ($subpages as $subpage) : ?>
                                                        <li>
                                                            <a href="<?php the_permalink($subpage->ID); ?>">
                                                                <?php echo $subpage->post_title; ?>
                                                            </a>
                                                            <?php if ($page->ID == 11): ?>
                                                                <?php
                                                                $terms = get_terms(array(
                                                                    'taxonomy' => get_field('typ_postu_podrzednego', $subpage->ID) . '_type',
                                                                    'hide_empty' => false,
                                                                    'parent' => 0,
                                                                    'orderby' => 'meta_value',
                                                                    'order' => 'ASC',
                                                                    'meta_key' => 'branch-letter'
                                                                ));
                                                                ?>
                                                                <?php if ($terms) : ?>
                                                                    <ul>
                                                                        <?php foreach ($terms as $term) : ?>
                                                                            <li>
                                                                                <a href="<?php echo get_term_link($term); ?>">
                                                                                    <?php echo $term->name; ?>
                                                                                </a>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php get_template_part('template-parts/content', 'none'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </main><!-- .site-main -->
    </div><!-- .content-area -->

<?php get_footer(); ?>