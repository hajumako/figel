<?php
/**
 * Template Name: Wiedza - artykuły
 *
 * Lista artykułów
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area <?php if ($wp_query->max_num_pages > 1) echo 'paged';?>">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper knowledge">
            <?php get_template_part('template-parts/side', 'siblings'); ?>

            <div class="news-list articles">
                <?php get_template_part('template-parts/content', 'articles'); ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
