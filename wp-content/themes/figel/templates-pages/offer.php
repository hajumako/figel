<?php
/**
 * Template Name: Oferta - główna
 *
 * Lista gałęzi głównych
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper offer">
            <div class="offer-list remove-padding">
            <?php
            $args = array(
                'post_type' => 'page',
                'post_parent' => $post->ID,
                'orderby' => 'meta_value',
                'order' => 'ASC',
                'meta_key' => 'branch-letter',
                'posts_per_page' => -1);
            $children = new WP_Query($args); ?>
            <?php while ($children->have_posts()): $children->the_post(); ?>
                <div class="box">
                    <a href="<?php the_permalink(); ?>">
                        <div class="image-wrapper">
                        <?php if (has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('product-list'); ?>
                        <?php endif; ?>
                        </div>
                        <h4><?php the_title(); ?></h4>
                    </a>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
