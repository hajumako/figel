<?php
/**
 * Template Name: Oferta - gałąź
 *
 * Lista podgałęzi danej gałęzi
 * 
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php get_template_part( 'template-parts/content', 'branch' ); ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
