<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="content-wrapper normal-page">
            <?php //get_template_part('template-parts/side', 'siblings'); ?>
            <?php include( locate_template('template-parts/side-siblings.php')); ?>
            <div class="normal-page-content">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <header class="entry-header">
                                <h2 class="title"><?php the_title(); ?></h2>
                                <div class="jobs-single">
                                    <p class="place">Miejsce pracy: <b><?php the_field('miejsce_pracy'); ?></b></p>
                                    <p class="date">Data dodania: <b><?php the_date_custom(); ?></b></p>
                                </div>

                            </header>
                            <div class="news-content">
                                <?php get_template_part('template-parts/content', 'flex'); ?>
                                <div class="share">
                                    <!--                                <a href="http://www.facebook.com/sharer/sharer.php?u=-->
                                    <?php //print(urlencode(get_permalink())); ?><!--&title=--><?php //print(urlencode(the_title())); ?><!--&description=-->
                                    <?php //print(urlencode(the_excerpt()) );?><!--&picture=--><?php //print(urlencode(the_post_thumbnail_url()));?><!--">facebook</a>-->
                                    <!--                                <a href="https://plus.google.com/share?url=--><?php //print(urlencode(get_permalink())); ?><!--">google +</a>-->

                                </div>
                            </div>
                        </article>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php get_template_part('template-parts/content', 'none'); ?>
                <?php endif; ?>
            </div>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
