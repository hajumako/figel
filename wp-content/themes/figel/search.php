<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<header class="page-header">
	<div class="page-title-wrapper">
		<h1 class="page-title"><?php echo 'Wyniki wyszukiwania dla: ', '<span>' . esc_html( get_search_query() ) . '</span>'; ?></h1>
	</div>
	<nav class="breadcrumbs">
		<a href="<?php echo bloginfo('url'); ?>">Główna</a>
		<span class="separator"></span>
		<span>Wyniki wyszukiwania</span>
	</nav>
</header>

<div id="primary" class="content-area <?php if ($wp_query->max_num_pages > 1) echo 'paged';?>">
	<main id="main" class="site-main" role="main">
		<div class="content-wrapper knowledge">
			<div class="news-list articles">
				<?php get_template_part('template-parts/content', 'search'); ?>
			</div>
		</div>
	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
