<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

$redirect = get_field('redirect', $post->ID);
if($redirect)
    header('Location: ' . $redirect);

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php get_template_part('template-parts/side', 'recent'); ?>

        <div class="content-wrapper news-details">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <p class="date"><?php the_date(); ?></p>
                        </header>
                        <div class="news-content">
                            <div class="share">
                                <!--                                <a href="http://www.facebook.com/sharer/sharer.php?u=-->
                                <?php //print(urlencode(get_permalink())); ?><!--&title=--><?php //print(urlencode(the_title())); ?><!--&description=-->
                                <?php //print(urlencode(the_excerpt()) );?><!--&picture=--><?php //print(urlencode(the_post_thumbnail_url()));?><!--">facebook</a>-->
                                <!--                                <a href="https://plus.google.com/share?url=--><?php //print(urlencode(get_permalink())); ?><!--">google +</a>-->

                            </div>
                            <?php get_template_part('template-parts/content', 'flex'); ?>
                        </div>
                    </article>
                <?php endwhile; ?>
            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
