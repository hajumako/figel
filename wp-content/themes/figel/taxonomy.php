<?php
/**
 *
 */

get_header(); ?>

<?php get_template_part('template-parts/head', 'title'); ?>

<div id="primary" class="content-area <?php if ($wp_query->max_num_pages > 1) echo 'paged-products';?>">
    <main id="main" class="site-main" role="main">
        <?php get_template_part( 'template-parts/content', 'products' ); ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
