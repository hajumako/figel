<?php
/**
 * Created by PhpStorm.
 * User: Konrado
 * Date: 2016-08-19
 * Time: 11:01
 */

add_action('admin_menu', 'branches_management');
add_action('created_term', 'figel_created_term', 10, 3);
add_action('save_post', 'figel_save_post', 100000);


function branches_management()
{
    add_menu_page('Zarządzanie gałęziami', 'Zarządzanie gałęziami', 'manage_options', 'branches_management', 'manager_options', 'dashicons-networking', 24);
}

function manager_options()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    if (isset($_POST['b'])) {
        foreach ($_POST['b'] as $branch) {
            $b_changed = update_post_meta($branch['id'], 'branch-letter', strtoupper($branch['branch-letter']));
            update_post_meta($branch['id'], 'branch-manager', $branch['branch-manager']);
            if ($b_changed)
                update_post_meta($branch['id'], 'updated_in_accounting_system', '');
            elseif ($branch['updated_in_accounting_system'] == 'on')
                update_post_meta($branch['id'], 'updated_in_accounting_system', '1');

            foreach ((array)$branch['sb'] as $subbranch) {
                $sb_changed = update_term_meta($subbranch['id'], 'branch-letter', strtoupper($subbranch['branch-letter']));
                update_term_meta($subbranch['id'], 'parent-branch-manager', $branch['branch-manager']);
                update_term_meta($subbranch['id'], 'branch-manager', $subbranch['branch-manager']);

                if ($sb_changed || $b_changed) {
                    update_term_meta($subbranch['id'], 'updated_in_accounting_system', '');
                } elseif ($subbranch['updated_in_accounting_system'] == 'on') {
                    update_term_meta($subbranch['id'], 'updated_in_accounting_system', '1');
                }

                foreach ((array)$subbranch['sbb'] as $subsubbranch) {
                    $ssb_changed = update_term_meta($subsubbranch['id'], 'branch-letter', strtoupper($subsubbranch['branch-letter']));

                    if ($ssb_changed || $sb_changed || $b_changed) {
                        update_term_meta($subsubbranch['id'], 'updated_in_accounting_system', '');
                    } elseif ($subsubbranch['updated_in_accounting_system'] == 'on') {
                        update_term_meta($subsubbranch['id'], 'updated_in_accounting_system', '1');
                    }
                }
            }
        }
    }

    global $wpdb;

    $search_query = 'SELECT ID, post_title, post_content FROM ei4hi_posts WHERE post_type = "acf-field-group" AND post_title LIKE %s AND post_status = "publish" ORDER BY post_title ASC';

    $like = 'Filtr %';
    $results = $wpdb->get_results($wpdb->prepare($search_query, $like), ARRAY_A);
    foreach ($results as $key => $array) {
        $meta[] = unserialize($array['post_content']);
        $quote_ids[] = $array['ID'];
    }

    $managers = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'product_manager',
            'orderby' => 'post_title',
            'order' => 'ASC'
        ));

    $options = '';
    foreach ($managers as $manager) {
        $options .= '<option value="' . $manager->ID . '">' . $manager->post_title . '</option>';
    }

    $branches = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'page',
            'post_parent' => 11,
            'post_status' => 'any',
            'orderby' => 'meta_value',
            'order' => 'ASC',
            'meta_key' => 'branch-letter'
        ));

    echo '<div class="wrapper">
            <form method="post">';
    foreach ($branches as $i => $branch) {
        $bl = get_post_meta($branch->ID, 'branch-letter', true);
        $bm = get_post_meta($branch->ID, 'branch-manager', true);
        $d_bl = $bl != '' ? $bl : '<span class="err">?</span>';
        $u_b = get_post_meta($branch->ID, 'updated_in_accounting_system', true);

        $branch->child_post_type = get_field('typ_postu_podrzednego', $branch->ID);
        echo '
            <div class="b">
                <input type="hidden" name="b[' . $i . '][id]" value="' . $branch->ID . '"/>
                <header>
                    <div class="editable letter">
                        <span>' . $d_bl . '</span>
                        <input type="text" name="b[' . $i . '][branch-letter]" id="branch-letter-' . $branch->ID . '" value="' . $bl . '" maxlength="1"/>
                    </div>
                    <h4>' . $branch->post_title . '</h4>
                    <p>Menadżer: </p>
                    <div class="editable manager">           
                        <select name="b[' . $i . '][branch-manager]" id="branch-manager-' . $branch->ID . '">
                            <option value="0" ' . ($bm == 0 ? 'selected="selected"' : '') . '>Brak</option>';
        $bm_def = 0;
        $bm_name = '';
        foreach ($managers as $m) {
            if ($bm == $m->ID) {
                $bm_def = $m->ID;
                $bm_name = $m->post_title;
                echo '<option value="' . $m->ID . '" selected="selected">' . $m->post_title . '</option>';
            } else
                echo '<option value="' . $m->ID . '">' . $m->post_title . '</option>';

        }
        echo '                
                        </select>
                        <span>' . ($bm_name != '' ? $bm_name : '<span class="err">Brak!</span>') . '</span>  
                    </div>
                    <button type="button" class="edit">Zmień</button>
                    ' . ($u_b == 1 ? '' : '<span class="update"><input type="checkbox" name="b[' . $i . '][updated_in_accounting_system]"/>Zmiany wprowadzone w systemie księgowym?</span>') . '
                    <input type="hidden" name="def" class="def_val" value="' . $bm_def . '"/>
                </header>';

        $terms = get_terms(array(
            'taxonomy' => get_field('typ_postu_podrzednego', $branch->ID) . '_type',
            'hide_empty' => false,
            'parent' => 0,
            'orderby' => 'meta_value',
            'order' => 'ASC',
            'meta_key' => 'branch-letter'
        ));
        if (!count($terms)) {
            list_filters($meta, $branch, $results, null);
        } else {
            foreach ($terms as $j => $term) {
                $sbl = get_term_meta($term->term_id, 'branch-letter', true);
                $sbm = get_term_meta($term->term_id, 'branch-manager', true);
                $d_sbl = $d_bl . ($sbl != '' ? $sbl : '<span class="err">?</span>');
                $u_sb = get_term_meta($term->term_id, 'updated_in_accounting_system', true);
                $invisible = get_field('ukryta', get_field('typ_postu_podrzednego', $branch->ID) . '_type' . '_' . $term->term_id);

                echo '
                <div class="sb' . ($invisible ? ' invisible' : '') . '">
                    <input type="hidden" name="b[' . $i . '][sb][' . $j . '][name]" value="' . $term->name . '"/>
                    <input type="hidden" name="b[' . $i . '][sb][' . $j . '][id]" value="' . $term->term_id . '"/>
                    <header>
                        <div class="editable letter">
                            <span>' . $d_sbl . '</span>
                        <input type="text" name="b[' . $i . '][sb][' . $j . '][branch-letter]" id="subbranch-letter-' . $term->term_id . '" value="' . $sbl . '" maxlength="1"/>
                        </div>
                        <h4>' . $term->name . '</h4>
                        <p>Menadżer: </p>
                        <div class="editable manager">
                            <select name="b[' . $i . '][sb][' . $j . '][branch-manager]" id="subbranch-manager-' . $term->term_id . '">
                                <option value="0" ' . ($sbm == 0 ? 'selected="selected"' : '') . '>Brak</option>';
                $sbm_def = 0;
                $sbm_name = '';
                foreach ($managers as $m) {
                    if ($sbm == $m->ID) {
                        $sbm_def = $m->ID;
                        $sbm_name = $m->post_title;
                        echo '<option value="' . $m->ID . '" selected="selected">' . $m->post_title . '</option>';
                    } else
                        echo '<option value="' . $m->ID . '">' . $m->post_title . '</option>';

                }
                echo '                
                            </select>
                            <span>' . ($sbm_name != '' ? $sbm_name : '<span class="err">Brak!</span>') . '</span>             
                        </div>
                        <button type="button" class="edit">Zmień</button>
                        ' . ($u_sb == 1 ? '' : '<span class="update"><input type="checkbox" name="b[' . $i . '][sb][' . $j . '][updated_in_accounting_system]"/>Zmiany wprowadzone w systemie księgowym?</span>') . '
                        <input type="hidden" name="def" class="def_val" value="' . $sbm_def . '"/>
                    </header>';

                list_filters($meta, $branch, $results, $term);

                $subterms = get_terms(array(
                    'taxonomy' => get_field('typ_postu_podrzednego', $branch->ID) . '_type',
                    'hide_empty' => false,
                    'parent' => $term->term_id,
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'meta_key' => 'branch-letter'
                ));
                foreach ($subterms as $k => $subterm) {
                    $ssbl = get_term_meta($subterm->term_id, 'branch-letter', true);
                    $d_ssbl = $d_sbl . ($ssbl != '' ? $ssbl : '<span class="err">?</span>');
                    $u_ssb = get_term_meta($subterm->term_id, 'updated_in_accounting_system', true);
                    $invisible = get_field('ukryta', get_field('typ_postu_podrzednego', $branch->ID) . '_type' . '_' . $subterm->term_id);

                    echo '
                    <div class="sbb' . ($invisible ? ' invisible' : '') . '">
                        <input type="hidden" name="b[' . $i . '][sb][' . $j . '][sbb][' . $k . '][id]" value="' . $subterm->term_id . '"/>
                        <header>
                            <div class="editable letter">
                                <span>' . $d_ssbl . '</span>
                                <input type="text" name="b[' . $i . '][sb][' . $j . '][sbb][' . $k . '][branch-letter]" id="subsubbranch-letter-' . $subterm->term_id . '" value="' . $ssbl . '" maxlength="1"/>          
                            </div>
                            <h4>' . $subterm->name . '</h4>
                            <button type="button" class="edit">Zmień</button>
                            ' . ($u_ssb == 1 ? '' : '<span class="update"><input type="checkbox" name="b[' . $i . '][sb][' . $j . '][sbb][' . $k . '][updated_in_accounting_system]"/>Zmiany wprowadzone w systemie księgowym?</span>') . '
                        </header>
                    </div>';
                }
                echo '</div>';
            }
        }
        echo '</div>';
    }
    echo '
                <div class="nav">
                    <button type="button" id="show">Pokaż/ukryj filtry</button>
                    <input type="submit" name="Submit" class="save" value="Zapisz" >
                    <input type="hidden" name="action" value="update" >
                </div>
            </form>
        </div>';
}

function figel_created_term($term_id, $tt, $taxonomy)
{
    if (preg_match('/_type$/', $taxonomy)) {
        add_term_meta($term_id, 'branch-letter', '', false);
        add_term_meta($term_id, 'updated_in_accounting_system', '');
    }
}

function figel_save_post($post)
{
    if ($_POST['parent_id'] == 11) {
        add_post_meta($_POST['post_ID'], 'branch-letter', '', true);
        add_post_meta($_POST['post_ID'], 'updated_in_accounting_system', '');
    }
}

function list_filters($meta, $branch, $results, $term)
{
    echo '<div class="filters">';
    foreach ($meta as $l => $m) {
        $echos = [];
        foreach ($m['location'] as $x => $rule) {
            $echos[$x] = true;
            foreach ($rule as $condition) {
                if ($condition['param'] == 'post_type') {
                    if ($condition['operator'] == "!=") {
                        if ($condition['value'] != $branch->child_post_type)
                            $echos[$x] &= true;
                        else
                            $echos[$x] &= false;
                    } elseif ($condition['operator'] == "==") {
                        if ($condition['value'] == $branch->child_post_type)
                            $echos[$x] &= true;
                        else
                            $echos[$x] &= false;
                    }
                } elseif ($condition['param'] == 'post_taxonomy') {
                    if ($condition['operator'] == "!=") {
                        if ($condition['value'] != $term->taxonomy . ':' . $term->slug)
                            $echos[$x] &= true;
                        else
                            $echos[$x] &= false;
                    } elseif ($condition['operator'] == "==") {
                        if ($condition['value'] == $term->taxonomy . ':' . $term->slug)
                            $echos[$x] &= true;
                        else
                            $echos[$x] &= false;
                    }
                }
            }
        }
        $e = false;
        foreach ($echos as $echo)
            $e |= $echo;
        if ($e)
            echo '<p>' . $results[$l]['post_title'] . '</p>';
    }
    echo '</div>';
}


