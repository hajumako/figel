/**
 * Created by Konrado on 2016-08-19.
 */

jQuery(document).ready(function () {
    $ = jQuery;
    $('button.edit').click(function () {
        $this = $(this);
        if($this.text() === 'Zmień') {
            $this.text('Anuluj');
            $('.editable span', $this.parent()).hide();
            $('.editable input', $this.parent()).show();
            $('.editable select', $this.parent()).show();
        } else {
            $this.text('Zmień');
            $('.editable span', $this.parent()).show();
            $('.editable input', $this.parent()).hide();
            $('.editable select', $this.parent()).hide();

            df = $('.def_val', $this.parent()).val();
            $('select', $this.parent()).val(df);
        }
    });
    $('button#show').click(function (e) {
        $('.filters').toggleClass('show');
    })
})