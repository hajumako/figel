<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
	<header class="page-header">
		<div class="page-title-wrapper">
			<h1 class="page-title">404</h1>
		</div>
	</header>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<h1 class="page-title"><?php pll_e('Przepraszamy');?></h1>
			<section class="error-404 not-found">
				<div class="page-content">
					<?php
					$currentlang = get_bloginfo('language');
					if ($currentlang == "en-GB") {
						$id = 42;
					} else {
						$id = 42;
					}
					?>
					<p><?php pll_e('Podany adres strony nie istnieje. Prosimy o upewnienie się czy został wpisany poprawnie. Jeżeli problem będzie się powtarzać'); ?> <span><a href="<?php echo get_permalink($id);?>"><?php pll_e('skontaktuj');?></a></span> z nami.</spav></p>
					<a class="more" href="<?php echo home_url();?>"><?php pll_e('Powrót');?></a>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- .site-main -->


	</div><!-- .content-area -->

<?php get_footer(); ?>
